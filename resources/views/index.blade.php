<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Alimento da Mente</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6797420998078967",
            enable_page_level_ads: true
        });
    </script>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' rel='stylesheet'/> -->
    <!-- Font Awesome & Pixeden Icon Stroke icon font-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css">
    <!-- Google fonts - Roboto Condensed & Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:300,400">
    <!-- lightbox-->
    <link rel="stylesheet" href="css/lightbox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <link rel="shortcut icon" href={{asset("favicon.png")}}>
  </head>
  <body class="home">
    <!-- navbar-->
    <header class="header">
      <div role="navigation" class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header"><a href="http:\\{!! env('MAIN_DOMAIN') !!}\adm\site_adm\public" class="navbar-brand">Alimento da Mente</a>
            <div class="navbar-buttons">
              <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
            </div>
          </div>
          <div id="navigation" class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
              <li class="active"><a href="{!! route('index') !!}">Home</a></li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Instituições <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  @forelse($schools as $school)
                    <li><a href="http://{!! $school->slug.'.'.env('MAIN_DOMAIN') !!}">{!! $school->name !!}</a></li>
                  @empty
                    <li><a href="#">Nenhum Instituição Cadastrada</a></li>
                  @endforelse
                </ul>
              </li>
              <li><a href="{!! route('contact') !!}">Contato</a></li>
              @role('super_admin|admin')
                <li><a href="{!! route('home') !!}">Painel Administrativo</a></li>
              @endrole
            </ul>
          </div>
        </div>
      </div>
    </header>
    <div id="carousel-home" data-ride="carousel" class="carousel slide carousel-fullscreen carousel-fade">
      <!-- Indicators-->
      <ol class="carousel-indicators">
        <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-home" data-slide-to="1"></li>
        <li data-target="#carousel-home" data-slide-to="2"></li>
        <li data-target="#carousel-home" data-slide-to="3"></li>
      </ol>
      <!-- Wrapper for slides-->
      <div role="listbox" class="carousel-inner">
        <div style="background-image: url('img/primeiro.png');" class="item active">
          <div class="overlay"></div>
        </div>
        <div style="background-image: url('img/segundo.png');" class="item">
          <div class="overlay"></div>
        </div>
        <div style="background-image: url('img/terceiro.png');" class="item">
          <div class="overlay"></div>
        </div>
        <div style="background-image: url('img/quarto.png');" class="item">
          <div class="overlay"></div>
        </div>
      </div>
    </div>
    <section class="background-gray-lightest negative-margin">
      <div class="container">
        <h1> O que é o projeto Alimento da Mente?</h1>
        <p class="lead" align="justify">O ADM (Alimento da Mente) é um projeto pioneiro na área que busca dar novas possibilidades ao aprendizado através da tecnologia que cresce à nossa volta constantemente. Através de um sistema robusto e inovador nós reunimos três grandes sistemas voltados ás bases da educação, o Conectados pela Leitura, o Conectados pela Matemática e o Conectados pelas Linguagens e Códigos, sempre com versões desenvolvidas especificamente para ás instituições de ensino em suas individualidades e com administradores responsáveis pela manutenção do projeto devidamente treinados em cada escola em que o ADM se instalar, com o objetivo de alavancar o aprendizado do corpo estudantil brasileiro.</p>

        <h1> O que é o Conectados pela Leitura?</h1>
        <p class="lead" align="justify">O Conectados LT (Conectados pela Leitura) é um projeto que busca incentivar a leitura através do aluguel dos livros disponíveis na biblioteca da escola, este sistema consiste em trazer os livros para mais perto do estudante, através da tecnologia, tornando possível que ele  possa consultar os livros do sistema e saber se o livro desejado está disponível na biblioteca da sua escola com uma rápida pesquisa além de poder aluga-lo ali mesmo a fazer a renovação do emprestimo pela própria internet, dessa maneira não ajudando somente o estudante, mas também os bibliotecários que terão um sistema facilitado para fazer o controle dos livros disponíveis, assim como dos alugados. Dentro da busca por livros e autores o aluno terá disponível a imagem da capa do livro e uma breve sinopse, além de poder avaliar o livro que leu após a sua devolução.</p>

        <h1> O que é o Conectados pela Matemática?</h1>
        <p class="lead" align="justify">O Conectados MT (Conectados pela matemática) é um projeto que visa o aprendizado em matemática e a prática de exercícios de forma bem simples. Buscamos através do nosso projeto, incentivar a resolução de exercícios matemáticos fora do contexto de sala de aula, para que se torne um hábito como ler um livro por exemplo. O projeto Conectados MT vai além do ensino, também envolve o quesito de vencer barreiras como: medo, preconceito e o receio que as pessoas têm da matemática. Através de uma plataforma bem interativa e simples, desejamos o melhor aprendizado a todos os usuários.</p>

        <h1> O que é o Conectados pelas Linguagens e Códigos?</h1>
        <p class="lead" align="justify">O Conectados LC (Conectados pelas linguagens e códigos) é um projeto que visa o incentivo da leitura dos jovens do Estado do Ceará, visto que existe um desinteresse constante pela leitura na juventude atual. Esse projeto irá trazer à juventude a oportunidade de construir uma melhor criticidade e, além disso, poderá alimentar seu intelecto nos três anos do ensino médio. Dessa forma, ele estará preparado para um futuro vestibular e conseguirá incluir-se no âmbito profissional. Informações do projeto: O público alvo desse projeto são jovens do Estado do Ceará de 14 a 25 anos que estão no ensino médio. O projeto será realizado tanto em escolas profissionalizantes quanto em escolas regulares, havendo parcerias entre a Secretaria de Educação do Estado e as gestões das escolas.</p>
      </div>
    </section>
    <section class="section--padding-bottom-small">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <div class="post">
              <div class="image"><img src="img/equipe_adm.jpg" alt="" class="img-responsive"></div>
              <h3>Mesa administradora Alimento da Mente</h3>
              <p class="post__intro"> Da esquerda para direita: <a href="https://www.facebook.com/matheuslemos70" class="external">Matheus Lemos</a>, <a href="https://www.facebook.com/adri.erijonhson" class="external">Adri Silva</a>, <a href="https://www.facebook.com/clarisse.lima.3" class="external">Clarisse Lima</a>, <a href="https://www.facebook.com/alexdevcoder" class="external">Pedro Alexandre</a>, <a href="https://www.facebook.com/vitoriaelen.ferreirasa" class="external">Vitória Elen</a> e <a href="https://www.facebook.com/jailton.victor.9" class="external">Jailton Victor</a>.</p>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="post">
              <div class="image"><img src="img/blog5.jpg" alt="" class="img-responsive"></div>
              <h3>Professores coordenadores do projeto inicial</h3>
              <p class="post__intro" align="justify"> Da esquerda para direita: <a href="https://www.facebook.com/wellington.s.saraiva" class="external">Wellington Saraiva</a> (Professor de Artes), <a href="https://www.facebook.com/karoline.holanda" class="external">Karoline Holanda</a> (Professora de Literatura e Redação), <a href="https://www.facebook.com/jalysonn" class="external">Jalyson Vieira</a> (Professor e Coordenador do curso técnico em Informática), <a href="https://www.facebook.com/maria.nildete" class="external">Maria Nildete</a> (Professora do curso técnico em Informática), <a href="https://www.facebook.com/alexandre.nicholas.5" class="external">Alexandre Nicholas</a> (Professor de Geometria), <a href="https://www.facebook.com/genivalsa.santos" class="external">Genival Sá</a> (Professor de Física e Coordenador da EEEP. Professor Onélio Porto), Mariza Honorato (Professora de Língua portuguesa e bibliotecária na EEEP. Professor Onélio Porto) e <a href="https://www.facebook.com/samia.queiroz.92" class="external">Sâmia Queiroz</a> (Professora de Filosofia e coordenadora da EEEP. Professor Onélio Porto).</p>
              
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-4">
            <div class="post">
              <div class="image"><img src="img/blog3.jpg" alt="" class="img-responsive"></div>
              <h3>Informática 2016-2018 Onélio Porto</h3>
              <p class="post__intro" align="justify">Esta foi a turma responsável por criar, apoiar e lançar o projeto Alimento da Mente em 2018 inicialmente no Onélio Porto. Composição da turma: <a href="https://www.facebook.com/adri.erijonhson" class="external">Adri Silva</a>, <a href="https://www.facebook.com/laysa.rodrigues.182" class="external">Ana Laysa</a>, <a href="https://www.facebook.com/matheuslemos70" class="external">Matheus Lemos</a>, <a href="https://www.facebook.com/Aureliofellix" class="external">Aurélio Felix</a>, <a href="https://www.facebook.com/profile.php?id=100011401366676" class="external">Carlos Henrique</a>, <a href="https://www.facebook.com/carlinhos.delima.3" class="external">Carlos Silva</a>, <a href="https://www.facebook.com/clarisse.lima.3" class="external">Clarisse Lima</a>, <a href="https://www.facebook.com/profile.php?id=100004852126156" class="external">Emanoel Lopes</a>, <a href="https://www.facebook.com/Zezinho.15" class="external">Fernando José</a>, <a href="https://www.facebook.com/erick.rodrigues.5055233" class="external">Erick Rodrigues</a>, <a href="https://www.facebook.com/rogerin247" class="external">Rogério de Oliveira</a>, <a href="https://www.facebook.com/profile.php?id=100014711988399" class="external">Gustavo Nascimento</a>, <a href="https://www.facebook.com/harion.lemos" class="external">Harion Lemos</a>, <a href="https://www.facebook.com/italo.anderson.940" class="external">Ítalo Anderson</a>, <a href="https://www.facebook.com/ithamar.luis" class="external">Ithamar Luis</a>, <a href="https://www.facebook.com/jailton.victor.9" class="external">Jailton Victor</a>, <a href="https://www.facebook.com/john.kevem.14" class="external">Jonh Kevin</a>, <a href="https://www.facebook.com/RobsonAlmeidaS12" class="external">José Robson</a>, <a href="https://www.facebook.com/profile.php?id=100011480992697" class="external">Wiliam Linhares</a>, <a href="https://www.facebook.com/manuele.sousa.1" class="external">Maria da Graça</a>, <a href="https://www.facebook.com/rayane.santos.3914" class="external">Maria Rayane</a>, <a href="https://www.facebook.com/anammari2" class="external">Mariana Cardoso</a>, <a href="https://www.facebook.com/matheus.vitor.165033" class="external">Matheus Vitor</a>, <a href="https://www.facebook.com/alexdevcoder" class="external">Pedro Alexandre</a>, <a href="https://www.facebook.com/raina.rodrguesr" class="external">Raina Rodrigues</a>, <a href="https://www.facebook.com/samir.alves.9279" class="external">Samir Alves</a> e <a href="https://www.facebook.com/vitoriaelen.ferreirasa" class="external">Vitória Elen</a>.</p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="post">
              <div class="image"><img src="img/blog2.jpg" alt="" class="img-responsive"></div>
              <h3>EEEP. Professor Onélio Porto</h3>
              <p class="post__intro" align="justify"> Aqui foi a escola onde o projeto ADM e os sistemas que o pompõe foram idealizados, criados e lançados.</p>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="post">
              <div class="image"><img src="img/blog5.jpg" alt="" class="img-responsive"></div>
              <h3>Equipe idealizadora do ADM</h3>
              <p class="post__intro" align="justify">Da esquerda para direita: <a href="https://www.facebook.com/matheuslemos70" class="external">Matheus Lemos</a> (CEO do ADM), <a href="https://www.facebook.com/adri.erijonhson" class="external">Adri Silva</a> (Chefe da equipe de programação do ADM), <a href="https://www.facebook.com/vitoriaelen.ferreirasa" class="external">Vitória Elen</a> (Idealizadora do Conectados LC), <a href="https://www.facebook.com/clarisse.lima.3" class="external">Clarisse Lima</a> (Idealizadora do Conectados MT), <a href="https://www.facebook.com/wellington.s.saraiva" class="external">Wellington Saraiva</a> (Idealizador do Conectados LT) e <a href="https://www.facebook.com/karoline.holanda" class="external">Karoline Holanda</a> (Idealizadora da únião dos três projetos e coordenadora do ADM no Onélio Porto).</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--   *** SERVICES ***-->
    <section class="background-gray-lightest">
      <div class="container clearfix">
        <div class="row services">
          <div class="col-md-12">
            <h2>Alimento da Mente na sua escola</h2>
            <p class="lead margin-bottom--medium" align="justify"> Gostou do Alimento da Mente? Quer ele em sua escola? Contacte nossa equipe e obtenha já uma versão própria desse projeto inovador, e deixe sua instituição mais organizada e informatizada, dessa forma ajudando na formação educacional dos alunos.</p>
          </div>
        </div>
      </div>
    </section>
    <!--   *** SERVICES END ***-->
    <!-- portfolio-->
    <section id="portfolio" class="section--no-padding-bottom">
      <div class="container">
        <div class="row">
          <div class="col-md-12 text-center">
            <h1>Galeria</h1>
            <p class="lead margin-bottom--big">Principais momentos do ADM</p>
          </div>
        </div>
      </div>
      <div class="container-fluid">
        <div class="row no-space">
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-1.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 1"><img src="img/portfolio-1.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-21.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 2"><img src="img/portfolio-21.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-3.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 3"><img src="img/portfolio-3.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-4.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 4"><img src="img/portfolio-4.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-5.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 5"><img src="img/portfolio-5.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-6.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 6"><img src="img/portfolio-6.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-7.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 7"><img src="img/portfolio-7.jpg" alt="" class="img-responsive"></a></div>
          </div>
          <div class="col-lg-3 col-sm-4 col-xs-6">
            <div class="box"><a href="img/portfolio-8.jpg" title="" data-lightbox="portfolio" data-title="Portfolio image 8"><img src="img/portfolio-8.jpg" alt="" class="img-responsive"></a></div>
          </div>
        </div>
      </div>
    </section>
    <footer class="footer">
      <div class="footer__copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2018 - Todos os direitos reservados | <font color='#ef5295'>Desenvolvido pela Equipe ADM</font></p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Javascript files-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"> </script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/front.js"></script>
  </body>
</html>