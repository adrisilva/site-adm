<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Alimento da Mente</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <script>
        (adsbygoogle = window.adsbygoogle || []).push({
            google_ad_client: "ca-pub-6797420998078967",
            enable_page_level_ads: true
        });
    </script>
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome & Pixeden Icon Stroke icon font-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css">
    <!-- Google fonts - Roboto Condensed & Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:300,400">
    <!-- lightbox-->
    <link rel="stylesheet" href="css/lightbox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <link rel="shortcut icon" href={{asset("favicon.png")}}>
  </head>
  <body class="home">
    <!-- navbar-->
    <header class="header">
      <div role="navigation" class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header"><a href="http:\\{!! env('MAIN_DOMAIN') !!}" class="navbar-brand">Alimento da Mente</a>
            <div class="navbar-buttons">
              <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
            </div>
          </div>
          <div id="navigation" class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">Home</a></li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Conectados <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="http:\\{!! $school->slug.'.'.env('MAIN_DOMAIN').'/conectados_leitura/public' !!}">Conectados pela Leitura</a></li>
                  <li><a href="http:\\{!! $school->slug.'.'.env('MAIN_DOMAIN').'/conectados_linguagem/public' !!}">Conectados pelas Linguagens e Códigos</a></li>
                  <li><a href="http:\\{!! $school->slug.'.'.env('MAIN_DOMAIN').'/conectados_matematica/public' !!}">Conectados pela Matemática</a></li>
                </ul>
              </li>
              <li><a href="https://alimentodamente.com.br/adm/site_adm/public/contato">Contato</a></li>
            </ul>
          </div>
        </div>
      </div>
    </header>
    <!-- *** LOGIN MODAL ***_________________________________________________________
    -->
    <div id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true" class="modal fade">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" data-dismiss="modal" aria-hidden="true" class="close">×</button>
            <h4 id="Login" class="modal-title">Login unificado</h4>
          </div>
          <div class="modal-body">
            <form action="customer-orders.html" method="post">
              <div class="form-group">
                <input id="email_modal" type="text" placeholder="email" class="form-control">
              </div>
              <div class="form-group">
                <input id="password_modal" type="password" placeholder="password" class="form-control">
              </div>
              <p class="text-center">
                <button type="button" class="btn btn-primary"><i class="fa fa-sign-in"></i> Entrar</button>
              </p>
            </form>
            <p class="text-center text-muted">Não é registrado?</p>
            <p class="text-center text-muted"><strong> Procure o coordenador do Alimento da Mente na sua escola e cadastre-se para ter acesso á todos os conectados implantados na sua instituição de ensino.</strong></p>
          </div>
        </div>
      </div>
    </div>
    <!-- *** LOGIN MODAL END ***-->

    <div id="carousel-home" data-ride="carousel" class="carousel slide carousel-fullscreen carousel-fade">
      <!-- Indicators-->
      <ol class="carousel-indicators">
        @php $index = 0 @endphp
        @forelse($school->slides as $slide)
          @if($slide->active)
            <li data-target="#carousel-home" data-slide-to="{!! $index !!}" class="{!! $index == 0 ? 'active' : '' !!}"></li>
            @php $index += 1 @endphp
          @endif
        @empty
          <li data-target="#carousel-home" data-slide-to="0" class="active"></li>
        @endforelse
      </ol>
      <!-- Wrapper for slides-->
      <div role="listbox" class="carousel-inner">
        @php $first_slide = true @endphp
        @foreach($school->slides as $index => $slide)
          @if($slide->active)
            <div style="background-image: url({!! asset($slide->media->path.$slide->media->filename) !!});" class="slide_item item {!! ($first_slide ? 'active' : '') !!}">
              <div class="overlay"></div>
              <div class="carousel-caption">
                <h1 class="super-heading">{!! $slide->title !!}</h1>
                <p class="super-paragraph">{!! $slide->subtitle !!}</p>
              </div>
            </div>
            @php $first_slide = false @endphp
          @endif
        @endforeach
        @if(count($slides_count) == 0)
          <div style="background-image: url('img/carousel2.jpg')" class="no_slides item active">
            <div class="overlay"></div>
            <div class="carousel-caption">
              <h1 class="super-heading">Alimento da Mente</h1>
            </div>
          </div>
        @endif
        </div>
      </div>
    </div>

    <section class="background-gray-lightest negative-margin">
      <div class="container">
        <!-- Nome da escola -->
        <h2> {!! $school->content->title !!}</h2>
        
        <!-- Conteúdo da escola -->
        {!! $school->content->about !!}
      </div>
    </section>
    <section class="section--padding-bottom-small">
      <div class="container">
        <div class="row">
          
          @foreach($school->images as $index => $image)
            @if($image->active)
              <div class="col-sm-6">
                <div class="post">
                  <div class="image"><img src="{!! asset($image->media->path.''.$image->media->filename) !!}" alt="" class="img-responsive"></div>
                  <h3>{!! $image->title !!}</h3>
                  <p class="post__intro">{!! $image->subtitle !!}</p>
                </div>
              </div>
            @endif
          @endforeach

        </div>
      </div>
    </section>
    <!--   *** SERVICES ***-->
    <section class="background-gray-lightest">
      <div class="container clearfix">
        <div class="row services">
          <div class="col-md-12">
            <h2>Conectados</h2>
            <p class="lead margin-bottom--medium"> Conheça quais dos conectados do ADM existem na EEEP. Professor Onélio Porto e passe já a usá-los desfrutando de seus benefícios.</p>
            <div class="row">

              @if($school->lt_active)
                <div class="col-sm-4">
                  <div class="box box-services">
                    <div class="icon"><i class="pe-7s-alarm"></i></div>
                    <h4 class="services-heading">Conectados LT</h4>
                    <p>Sim! Conectados pela Leitura, aqui você pode consultar a biblioteca da sua escola sem sair do conforto da sua banda larga.</p>
                  </div>
                </div>
              @endif

              @if($school->lc_active)
              <div class="col-sm-4">
                <div class="box box-services">
                  <div class="icon"><i class="pe-7s-cloud"></i></div>
                  <h4 class="services-heading">Conectados LC</h4>
                  <p>Com certeza! O Conectados pelas Linguagens e Códigos não poderia ficar de fora do Onélio Porto.<!-- aqui você vai tornar-se num verdadeiro atleta olimpico correndo para ler todos os livros indicados e se aventurando nas correntezas das músicas e materiais em vídeo. --></p>
                </div>
              </div>
              @endif

              @if($school->mt_active)
              <div class="col-sm-4">
                <div class="box box-services">
                  <div class="icon"><i class="pe-7s-coffee"></i></div>
                  <h4 class="services-heading">Conectados MT</h4>
                  <p>O conectados pela Matemática vai tornar mais fácil essa tarefa tão difícil para muitos que é passar a gostar de matemática. </p>
                </div>
              </div>
              @endif

            </div>
          </div>
        </div>
      </div>
    </section>
    <!--   *** SERVICES END ***-->

    <footer class="footer">
      <div class="footer__copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2018 - Todos os direitos reservados | <font color='#ef5295'>Desenvolvido pela Equipe ADM</font></p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Javascript files-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"> </script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/front.js"></script><!-- substitute:livereload -->

  </body>
</html>