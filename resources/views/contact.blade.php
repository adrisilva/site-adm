<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Contato | Alimento da Mente</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Font Awesome & Pixeden Icon Stroke icon font-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/pe-icon-7-stroke.css">
    <!-- Google fonts - Roboto Condensed & Roboto-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:700|Roboto:300,400">
    <!-- lightbox-->
    <link rel="stylesheet" href="css/lightbox.min.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href="favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    <link rel="shortcut icon" href={{asset("favicon.png")}}>
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <div role="navigation" class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header"><a href="http:\\{!! env('MAIN_DOMAIN') !!}" class="navbar-brand">Alimento da Mente</a>
            <div class="navbar-buttons">
              <button type="button" data-toggle="collapse" data-target=".navbar-collapse" class="navbar-toggle navbar-btn">Menu<i class="fa fa-align-justify"></i></button>
            </div>
          </div>
          <div id="navigation" class="collapse navbar-collapse navbar-right">
            <ul class="nav navbar-nav">
              <li><a href="{!! route('index') !!}">Home</a></li>
              <li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">Instituições <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  @forelse($schools as $school)
                    <li><a href='http:\\{!! $school->slug.'.'.env('MAIN_DOMAIN') !!}'>{!! $school->name !!}</a></li>
                  @empty
                    <li><a href="#">Nenhum Instituição Cadastrada</a></li>
                  @endforelse
                </ul>
              </li>
              <li class="active"><a href="{!! route('contact') !!}">Contato</a></li>
              @role('super_admin|admin')
                <li><a href="{!! route('home') !!}">Painel Administrativo</a></li>
              @endrole
            </ul>
          </div>
        </div>
      </div>
    </header>
  
    <section class="background-gray-lightest">
      <div class="container">
        <div class="breadcrumbs">
          <ul class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Contato</li>
          </ul>
        </div>

        @if(session()->has('success'))
          <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <p>
              {!! session()->get('success') !!}
            </p>
          </div>
        @endif

        <h1>Contato</h1>
        <p class="lead">Tem alguma dúvida no uso do site? Alguma sugestão ou reclamação? Quer levar o Alimento da Mente para sua escola? Contate-nos através do nosso e-mail. </p>
      </div>
    </section>
    <section>  
      <div id="contact" class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="box-simple">
              <div class="icon"><i class="pe-7s-map-2"></i></div>
              <div class="content">
                <h4>Endereço</h4>
                <p>Em breve<br><!--Bairro--><br>Fortaleza - CE, <strong>Brasil</strong></p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="box-simple">
              <div class="icon"><i class="pe-7s-phone"></i></div>
              <div class="content">
                <h4>Telefone</h4>
                <p class="text-muted">Está com dúvidas? Nos ligue ou mande mensagem via WhatsApp</p>
                <p><strong>Em breve</strong></p>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="box-simple">
              <div class="icon"><i class="pe-7s-mail-open"></i></div>
              <div class="content">
                <h4>Suporte Online</h4>
                <p class="text-muted">Sinta-se à vontade para nos escrever um email.</p>
                <ul>
                  <li><strong><a href="mailto:">contato@alimentodamente.com.br</a></strong></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-8">
            <div class="box-simple">
              <div class="icon"><i class="pe-7s-pen"></i></div>
              <div class="content">
                <h4>Formulário de contato</h4>
                <p class="text-muted">Escreva aqui seu e-mail com sua dúvida, sugestão ou questionamento e nossa equipe lhe responderá o mais rápido possível.</p>
                <form method="POST" action="{!! url('email-contato') !!}">
                  @csrf
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="firstname">Nome</label>
                        <input id="nome" name="nome" type="text" class="form-control" required>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="lastname">Sobrenome</label>
                        <input id="sobrenome" name="sobrenome" type="text" class="form-control" required>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="email">E-mail</label>
                        <input id="email" name="email" type="text" class="form-control" required>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="subject">Assunto</label>
                        <input id="assunto" name="assunto" type="text" class="form-control" required>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="message">Mensagem</label>
                        <textarea id="mensagem" name="mensagem" class="form-control" required></textarea>
                      </div>
                    </div>
                    <div class="col-sm-12">
                      <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Enviar mensagem</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- <div id="map"> 
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3980.8866739843775!2d-38.560185485148054!3d-3.834492397210637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x7c751e35869dd01%3A0xe6160def30227899!2sEEEP+Professor+On%C3%A9lio+Porto!5e0!3m2!1spt-BR!2sbr!4v1508859749552" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div> -->
    <footer class="footer">
      <div class="footer__copyright">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <p>&copy; 2018 - Todos os direitos reservados | <font color='#ef5295'>Desenvolvido pela Equipe ADM</font></p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Javascript files-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.cookie.js"> </script>
    <script src="js/lightbox.min.js"></script>
    <script src="js/front.js"></script><!-- substitute:livereload -->
  </body>
</html>