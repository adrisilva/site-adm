@extends('adminlte::page')

@section('title', 'CMS | Home')

@section('content_header')
<h1>Home</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Home</a></li>
</ol>
@stop

@section('content')

<div class="box box-primary">

	<div class="box-body">

		@if(session()->has('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{!! session()->get('success') !!}
			</div>
		@endif
		
		@if(isset($errors) && count($errors) > 0)
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{$error}}</p>
				@endforeach
			</div>
		@endif
		
		<form action="{!! url('admin/edit/home') !!}" method="POST" class="form">
			@csrf
			@method('put')
			<div class="form-group">
				<label class="col-sm-2">Título</label>
				<div class="col-sm-10">
					<input type="text" name="title" value="{!! $school->content->title !!}" class="form-control">
				</div>
			</div>
			
			<br><br>
			<div class="form-group">
				<label class="col-sm-2">Texto</label>
				<div class="col-sm-10">
					<textarea id="summernote" name="about" class="form-control">
						{!! $school->content->about !!}
					</textarea>
				</div>
			</div>

		<div class="box-footer">
			<button class="btn btn-primary btn-block" type="submit">Salvar</button>
		</div>

	</form>
</div>

@section('js')
<script>
	$(document).ready(function() {
		$('#summernote').summernote();
	});
</script>
@stop

@stop