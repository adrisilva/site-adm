@extends('adminlte::page')

@section('title', 'Imagens')

@section('content_header')
<h1>Imagens</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Imagens</a></li>
</ol>
@stop

@section('content')

<div class="box box-primary">

	<div class="box-body">
		@if(session()->has('success'))
		<div class="alert alert-success">
			<button type="button" class="close" data-dismiss="alert">×</button>
			{!! session()->get('success') !!}
		</div>
		@endif

		<div class="table-responsive">

			<table class="table" id="datatables">
				<thead>
					<tr>
						<th>Título</th>
						<th>Imagem</th>
						<th>Status</th>
						<th>Editar</th>
					</tr>
				</thead>

				<tbody>
					@forelse($images as $image)
						<tr>
							<td>{!! $image->title !!}</td>
							<td><img src="{!! asset( $image->media->path.$image->media->filename) !!}" width=150 height=100></td>
							
							<td>
								<input type="checkbox" 
									data-toggle="toggle" 
									data-on="Habilitado" data-off="Desabilitado" 
									data-onstyle="success" 
									data-offstyle="danger"
									data-style="ios"
									data-id={!! $image->id !!}
									value="1" 
									class="switch" name="active"
									{!! $image->active ? 'checked' : '' !!}>
							</td>
							<td><a href="{!! url('admin/images/'.$image->id.'/edit') !!}" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
						</tr>
					@empty
					@endforelse
				</tbody>

			</table>
		</div>
	</div>

</div>

@section('js')
	@include('admin.includes.notifications')
    <script>
    	$(function() {

    		$('.switch').change(function() {
    			var id 	= $(this).data('id');
    			var url = '{!! url('admin/images/{id}/change-status') !!}'.replace('{id}', id);

    			$.ajax({
    				url: url,
    				method: 'POST',
    				data: {'_token': '{!! csrf_token() !!}'}
    			}).done(function(){
    				iziToast.success({
    					timeout: 2000,
				        resetOnHover: true,
				        transitionIn: 'flipInX',
				        transitionOut: 'flipOutX',
    					title: 'OK',
    					message: "Status da imagem alterado com sucesso",
    				});
    			});
    		})
    	})
	</script>
@stop

@stop