@extends('adminlte::page')

@section('title', 'CMS | Imagem')

@section('content_header')
<h1>Imagem</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Imagem</a></li>
	<li><a href="#">Editar</a></li>
</ol>
@stop

@section('content')

<div class="box box-primary">

	<div class="box-body">

		@if(session()->has('success'))
			<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">×</button>
				{!! session()->get('success') !!}
			</div>
		@endif
		
		@if(isset($errors) && count($errors) > 0)
			<div class="alert alert-danger">
				@foreach($errors->all() as $error)
				<p>{{$error}}</p>
				@endforeach
			</div>
		@endif
		
		<form action="{!! url('admin/images/'.$image->id.'/edit') !!}" method="POST" class="form" enctype="multipart/form-data">
			@csrf
			@method('put')
			<div class="form-group">
				<label class="col-sm-2">Título</label>
				<div class="col-sm-10">
					<textarea id="summernote_title" name="title" class="form-control">
						{!! $image->title !!}
					</textarea>
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-2">Sub-título</label>
				<div class="col-sm-10">
					<textarea id="summernote" name="subtitle" class="form-control">
						{!! $image->subtitle !!}
					</textarea>
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-2">Imagem</label>
				<div class="col-sm-10">
					<input type="file" class="dropify" data-default-file="{!! asset($image->media->path.$image->media->filename) !!}" name="image" id="image" data-show-loader="true" accept="image/*">
				</div>
			</div>
		
		</div>

		<div class="box-footer">
			<button class="btn btn-primary btn-block" type="submit">Salvar</button>
		</div>

	</form>
</div>

@section('js')
<script>
	$(document).ready(function() {
		$('#summernote_title').summernote();
		$('#summernote').summernote();
		$('#image').dropify();
	});
</script>
@stop

@stop