@extends('adminlte::page')

@section('title', 'Diretores')

@section('content_header')
<h1>Coordenadores</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Coordenadores</a></li>
</ol>
@stop

@section('content')

<div class="box box-primary">

	<div class="box-body">
		@include('admin.includes.alerts')

		<div class="table-responsive">

			<table class="table" id="datatables">
				<thead>
					<tr>
						<th>Nome</th>
						<th>E-Mail</th>
						<th>Telefone</th>
						<th>Escola responsável</th>
					</tr>
				</thead>

				<tbody></tbody>

			</table>
		</div>
	</div>
</div>

@section('js')
    <script>
    	$(document).ready(function() {
	    	var table = $('#datatables').DataTable({
	    		'language'	  : {
	    			'url' : 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
	    		},

	    		'paging'      : true,
	    		'lengthChange': false,
	    		'searching'   : true,
	    		'ordering'    : true,
	    		'info'        : false,
	    		'autoWidth'   : false,
	    		'serverSide'  : true,
	    		'ordering'	  : false,
	    		'processing'  : true,
	    		'columns': [
		    	 	{data: "name"},
		    	 	{data: 'email'},
		    	 	{data: 'phone'},
		    	 	{data: 'school.name'}
	    		 ],

	    		'ajax': {
	    			'url': '{!! route('director.get', request()->school) !!}',
	    			'type': 'GET',
	    		},

                'order': [[ 0, "asc" ]]
	    	});
	    })
	</script>
@stop

@stop