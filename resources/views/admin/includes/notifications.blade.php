<script>
    iziToast.settings({
        timeout: 2000,
        resetOnHover: true,
        transitionIn: 'flipInX',
        transitionOut: 'flipOutX',
    });
</script>

@if(session('success'))
<script>
    iziToast.success({
        title: 'OK',
        message: "{{session('success')}}",
    });
</script>
@endif