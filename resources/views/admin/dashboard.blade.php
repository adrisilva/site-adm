@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
	<h1>Dashboard</h1>
@stop

@section('content')
	@if(session()->has('success'))
	<div class="alert alert-success">
		<button type="button" class="close" data-dismiss="alert">×</button>
		{!! session()->get('success') !!}
	</div>
	@endif
@stop