<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Senha de acesso - Alimento da Mente Onélio Porto</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="10" cellspacing="0" width="100%"><tr><td>	
		<table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse; border: 1px solid #cccccc;">
			<tr>
				<td align="center" bgcolor="#70bbd9" style="padding: 40px 0 30px 0;">
					<img src="https://i.imgur.com/dm5hPmu.png" alt="Criando Mágica de E-mail" width="155" height="150" style="display: block;" />
					<h1 style="font-family: COURIER, Verdana;">Alimento da Mente <br>Onélio Porto</h1>
				</td>
			</tr>

			<tr>
				<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
 					<table border="0" cellpadding="0" cellspacing="0" width="100%">
  						<tr>
   							<td align="center">
    							<h2><font face="Times">Senha de acesso á plataforma</font></h2>
   							</td>
  						</tr>
  						<tr>
   							<td align="justify" style="line-height: 1.5;"><br>
    							<font size="4">&emsp;&emsp;Olá <i>{{$name}}</i>, nós da equipe Alimento da Mente agradecemos seu cadastro junto á sua instituição de ensino, estamos sempre trabalhando para melhorar sua experiência e aprendizado na nossa plataforma. Informamos que seu cadastro foi realizado e finalizado com sucesso, você já pode a partir de agora começar a aproveitar tudo o que o ADM propõe para sua instituição de ensino. Seguem abaixo seus dados de acesso, por segurança e comodidade, recomendamos que altere sua senha de acesso para outra de sua preferência logo no primeiro acesso. Lembramos que sua conta é totalmente pessoal, privada, intransferível, e que o mantimento ou cancelamento da mesma fica totalmente a rigor da coordenação do projeto na sua instituição de ensino. Qualquer dúvida contacte-nos através da página de contato do projeto, ou procure os coordenadores resposáveis pelo projeto no seu estabelecimento de educação. </font>
   							</td>
  						</tr>
  						<tr>
   							<td align="center">
   								<font size="4">
    							<br><b>Login: {{$email}}</b>
    							<br><b>Senha: {{$password}}</b>
    							</font>
   							</td>
  						</tr>
 					</table>
				</td>
			</tr>

			<tr>
				<td bgcolor="#ee4c50" style="padding: 20px 20px 20px 20px;">
					<!-- <font size="4"></font> -->
					<!-- <img src="imagens/logo_circular.png" width="120" height="120"> -->
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
  						<tr>
   							<td align="center">
    							<a href="https://www.alimentodamente.com.br"><img src="https://i.imgur.com/W53Brlg.png" width="150" height="150"></a>
   							</td>
   						</tr>
   						<tr>	
   							<td align="center">
    							<br><font size="4" color="#ffffff"><i><a href='https://www.facebook.com/matheuslemos70' style="color: #ffffff;">Matheus Lemos</a></i><br>CEO da equipe Alimento da Mente</font>
   							</td>
  						</tr>
 					</table>
				</td>
			</tr>
			
		</table>
	</tr></td></table>	
	</body>
</html>