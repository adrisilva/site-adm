@extends('adminlte::page')

@section('title', 'First Access')

@section('content_header')
    <h1>First Access</h1>
@stop

@section('content')
    @if(session()->has('success'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert">×</button>
            {!! session()->get('success') !!}
        </div>
    @endif

    <div class="box box-primary">

        <div class="box-body">
            @include('admin.includes.alerts')

            <div class="table-responsive">

                <table class="table" id="datatables">
                    <thead>
                    <tr>
                        <th>Escola</th>
                        <th>Total de alunos</th>
                        <th>Acessaram CL</th>
                        <th>Acessaram MT</th>
                        <th>Acessaram LC</th>
                    </tr>
                    </thead>

                    <tbody></tbody>

                </table>
            </div>
        </div>

    </div>
@section('js')
    <script>
        $(document).ready(function() {
            var table = $('#datatables').DataTable({
                'language'	  : {
                    'url' : 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
                },

                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'serverSide'  : true,
                'processing'  : true,

                'columns': [
                    {data: "school"},
                    {data: 'total_alunos'},
                    {data: 'mysql_leitura'},
                    {data: 'mysql_matematica'},
                    {data: 'mysql_linguagem'},
                ],

                'ajax': {
                    'url': '{!! route('school.first_access.count', request()->school) !!}',
                    'type': 'GET',
                },

                'order': [[ 0, "asc" ]]
            });
        });

    </script>
@stop
@stop