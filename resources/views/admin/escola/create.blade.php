@extends('adminlte::page')

@section('title', 'Escolas')

@section('content_header')
<h1>Cadastrar Escola</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Escolas</a></li>
	<li><a href="#">Cadastro</a></li>
</ol>
@stop

@section('content')

<div class="box">

	<div class="box-body">

		@include('admin.includes.alerts')
		
		<form action="{{ route('school.store', request()->school) }}" method="POST">
			@csrf
			
			<div class="form-group">
				<label class="col-sm-2">Nome da Escola</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" placeholder="Digite o nome da escola" name="name"  value="{{ old('name') }}">
				</div>
			</div>
				
			<br><br>
			<div class="form-group">
				<label class="col-sm-2">Slug da Escola</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" placeholder="Ex.: onelio, onelio_porto" name="slug"  value="{{ old('slug') }}">
				</div>
			</div>
			<br><br>

			<div class="form-group">
				<label class="col-sm-2">INEP</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" placeholder="Digite o endereço inep da escola" name="inep"  value="{{ old('inep') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Nome do Coordenador(a) do Projeto na Escola</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="Nome do Coordenador(a) do Projeto na Escola" name="diretor_name"  value="{{ old('diretor_name') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">CPF do Coordenador(a) do Projeto na Escola</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="CPF do Coordenador(a) do Projeto na Escola" name="cpf"  value="{{ old('cpf') }}" data-mask="000.000.000-00">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">E-Mail do Coodeandor(a) do Projeto na Escola</label>
				<div class="col-sm-9">
					<input type="email" class="form-control" placeholder="E-Mail do Coodeandor(a) do Projeto na Escola" name="diretor_email"  value="{{ old('diretor_email') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Telefone do Coodenador(a) do Projeto na Escola</label>
				<div class="col-sm-9">
					<input type="text" class="form-control phone_with_ddd" placeholder="Telefone do Coodenador(a) do Projeto na Escola" name="diretor_tel"  value="{{ old('diretor_tel') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">CEP</label>
				<div class="col-sm-9">
					<input type="text" class="form-control cep" placeholder="Digite o cep da escola" name="cep"  value="{{ old('cep') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Endereço</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="Digite o endereço da escola" name="endereco"  value="{{ old('endereco') }}">
				</div>
			</div>

			{{-- PROJETO CONECTADOS PELA LEITURA --}}
			<br><br>
			<div class="col-sm-10">
				<h4><u>Conectados Pela Leitura</u></h4>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Coordenador</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="Coordenador do Conectados pela Leitura" name="coordenador_name[]"  value="{{ old('coordenador_name[0]') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">E-Mail</label>
				<div class="col-sm-9">
					<input type="email" class="form-control" placeholder="Email do coordenador do Conectados pela Leitura" name="coordenador_email[]" value="{{ old('coordenador_email[0]') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">CPF</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="CPF" name="coordenador_cpf[]"  value="{{ old('coordenador_email[0]') }}" data-mask="000.000.000-00">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Telefone</label>
				<div class="col-sm-8">
					<input type="text" class="form-control phone_with_ddd" placeholder="Telefone do coordenador do Conectados pela Leitura" name="coordenador_telefone[]" value="{{ old('coordenador_telefone[0]') }}">
				</div>
			</div>

			{{-- PROJETO LINGUAGEM E CÓDIGOS --}}
			<br><br>
			<div class="col-sm-10">
				<h4><u>Conectados Pela Linguagem</u></h4>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Coordenador</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="Coordenador do Conectados pela Linguagem" name="coordenador_name[]" value="{{ old('coordenador_name[1]') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">E-Mail</label>
				<div class="col-sm-9">
					<input type="email" class="form-control" placeholder="Email do coordenador do Conectados pela Linguagem" name="coordenador_email[]" value="{{ old('coordenador_email[1]') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">CPF</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="CPF" name="coordenador_cpf[]"  value="{{ old('coordenador_email[1]') }}" data-mask="000.000.000-00">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Telefone</label>
				<div class="col-sm-9">
					<input type="text" class="form-control phone_with_ddd" placeholder="Telefone do coordenador do Conectados pela Linguagem" name="coordenador_telefone[]" value="{{ old('coordenador_telefone[1]') }}">
				</div>
			</div>

			{{-- PROJETO MATEMÁTICA --}}
			<br><br>
			<div class="col-sm-10">
				<h4><u>Conectados Pela Matemática</u></h4>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Coordenador</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="Coordenador do Conectados pela Matemática" name="coordenador_name[]" value="{{ old('coordenador_name[2]') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">E-Mail</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="Email do coordenador do Conectados pela Matemática" name="coordenador_email[]" value="{{ old('coordenador_email[2]') }}">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">CPF</label>
				<div class="col-sm-9">
					<input type="text" class="form-control" placeholder="CPF" name="coordenador_cpf[]"  value="{{ old('coordenador_email[2]') }}" data-mask="000.000.000-00">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-3">Telefone</label>
				<div class="col-sm-9">
					<input type="text" class="form-control phone_with_ddd" placeholder="Telefone do coordenador do Conectados pela Matemática" name="coordenador_telefone[]" value="{{ old('coordenador_telefone[2]') }}">
				</div>
			</div>
			
			<br><br>
			<div class="form-group">
				<div class="form-check">
					<label class="col-sm-4">Ativar Conectados pela Leitura</label>
					<div class="col-sm-8">
						<input type="checkbox" 
							data-toggle="toggle" 
							data-on="Sim" data-off="Não" 
							data-onstyle="success" 
							data-offstyle="danger"
							data-style="ios" 
							value="1" 
							class="switch" name="lt_active" checked>
					</div>
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<div class="form-check">
					<label class="col-sm-4">Ativar Conectados pela Linguagens e Códigos</label>
					<div class="col-sm-8">
						<input type="checkbox" 
							data-toggle="toggle" 
							data-on="Sim" data-off="Não" 
							data-onstyle="success" 
							data-offstyle="danger"
							data-style="ios"  
							value="1" 
							class="switch" name="lc_active" checked>
					</div>
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<div class="form-check">
					<label class="col-sm-4">Ativar Conectados pela Matemática</label>
					<div class="col-sm-8">
						<input type="checkbox" 
							data-toggle="toggle" 
							data-on="Sim" data-off="Não" 
							data-onstyle="success" 
							data-offstyle="danger"
							data-style="ios" 
							value="1" 
							class="switch" name="mt_active" checked>
					</div>
				</div>
			</div>
		</div>
	</div>

		<div class="box-footer">
			<button type="button" class="btn btn-success" onclick="history.go(-1)">Voltar</button>
			<button class="btn btn-primary">Salvar</button>
		</form>
	</div>
</div>


@stop