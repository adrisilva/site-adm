@extends('adminlte::page')
@section('title', 'Escolas')
@section('content_header')
	<h1>Escolas</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="#">Escolas</a></li>
	</ol>

@stop
@section('content')

	<div class="box box-primary">

		<div class="box-body">
			@include('admin.includes.alerts')

			<div class="table-responsive">

				<table class="table" id="datatables">
					<thead>
					<tr>
						<th>Nome</th>
						<th>INEP</th>
						<th>Data de Cadastro</th>
						<th>Editar</th>
						<th>Excluir</th>
					</tr>
					</thead>

					<tbody></tbody>

				</table>
			</div>
		</div>

		<div class="box-footer">
			<a href="{!! route('school.create', request()->school) !!}" class="btn btn-success">Nova Escola</a>
		</div>
	</div>

	<div class="modal modal-danger fade" id="modal-danger">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">PERIGO</h4>
				</div>
				<div class="modal-body">
					<p>Ao deletar essa escola, todos os dados atrelados a ela serão apagados!!! Inclusive os dados dos outros sistemas!</p>
				</div>
				<div class="modal-footer">
					<form action="" method="POST" id="form-delete">
						@method('DELETE')
						@csrf
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-outline">Confirmar</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->


@section('js')
	<script>
        $(document).ready(function() {
            var table = $('#datatables').DataTable({
                'language'	  : {
                    'url' : 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
                },

                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'serverSide'  : true,
                'processing'  : true,

                'columns': [
                    {data: "name"},
                    {data: 'inep'},
                    {data: 'renewal_date'},
                    {data: 'edit', render: renderBtnEdit},
                    {data: 'remove', render: renderBtnDelete}
                ],

                'ajax': {
                    'url': '{!! route('school.get', request()->school) !!}',
                    'type': 'GET',
                },

                'order': [[ 0, "asc" ]]
            });

            function renderBtnEdit(type, row, data){
                var url = '{!! route('school.edit', 'slug') !!}'.replace('slug', data.slug);
                return `<a href=${url} class='btn btn-primary'><i class='fa fa-edit'></i></a>`;
            }

            function renderBtnDelete(type, row, data){
                return `<button class='btn btn-danger btn-delete' onclick='modalDelete("${data.slug}")' data-slug="${data.slug}"><i class='fa fa-trash'></i></button>`;
            }
        });

		function modalDelete(slug){
			var action = '{!! route('school.delete', 'slug') !!}'.replace('slug', slug);
			$("#form-delete").attr("action", action);
			$('#modal-danger').modal('show');
		}

	</script>
@stop
@stop