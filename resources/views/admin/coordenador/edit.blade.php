@extends('adminlte::page')

@section('content_header')

    <h1>Editar Coordenador</h1>

    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Coordenador</a></li>
        <li><a href="">Editar</a></li>
    </ol>
@stop

@section('content')

    {!! Form::model($user, ['url' => ['admin/coordenadores/'.$user->id.'/editar'], 'method' => 'PUT']) !!}

    <div class="box box-primary">

        <div class="box-body">

            @include('admin.includes.alerts')

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', 'Nome', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('cpf', 'CPF', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('cpf', null,['class' => 'form-control', 'placeholder' => 'CPF', 'data-mask' => '000.000.000-00']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', 'E-mail', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('email', null,['class' => 'form-control', 'placeholder' => 'E-Mail']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', 'Telefone', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('phone', null,['class' => 'form-control phone_with_ddd', 'placeholder' => 'Telefone']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('password', 'Senha', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Senha']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" onclick="history.go(-1)">Voltar</button>
                    </div>
                    <div class="col-sm-10">
                        {!! Form::submit('Salvar', ['class' => 'btn btn-block btn-success']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {!! Form::close() !!}

@stop