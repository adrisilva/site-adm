@extends('adminlte::page')

@section('content_header')

    <h1>Cadastro Coordenador</h1>

    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="">Coordenador</a></li>
        <li><a href="">Cadastro</a></li>
    </ol>
@stop

@section('content')

    {!! Form::open(['url' => ['admin/coordenadores/novo'], 'method' => 'POST']) !!}

    <div class="box box-primary">

        <div class="box-body">

            @include('admin.includes.alerts')

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', 'Nome', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('name', null,['class' => 'form-control', 'placeholder' => 'Nome']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('cpf', 'CPF', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        <input type="text" name="cpf" class="form-control" data-mask="000.000.000-00" placeholder="CPF">
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', 'E-mail', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('email', null,['class' => 'form-control', 'placeholder' => 'E-Mail']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    {!! Form::label('name', 'Telefone', ['class' => 'col-sm-2']) !!}

                    <div class="col-sm-10">
                        {!! Form::text('phone', null,['class' => 'form-control phone_with_ddd', 'placeholder' => 'Telefone']) !!}
                    </div>
                </div>
            </div>
            <p></p>

            <div class="row">
                <div class="form-group">
                    <div class="col-sm-2">
                        <button type="button" class="btn btn-primary" onclick="history.go(-1)">Voltar</button>
                    </div>
                    <div class="col-sm-10">
                        {!! Form::submit('Salvar', ['class' => 'btn btn-block btn-success']) !!}
                    </div>
                </div>
            </div>

        </div>
    </div>

    {!! Form::close() !!}

@stop