@extends('adminlte::page')

@section('title', 'Coodenadores')

@section('content_header')
    <h1>Coodenadores</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Coodenadores</a></li>
    </ol>
@stop

@section('content')

    <div class="box box-primary">

        <div class="box-body">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    {!! session()->get('success') !!}
                </div>
            @endif

            <div class="table-responsive">

                <table class="table" id="datatables">
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>E-Mail</th>
                        <th>Telefone</th>
                        <th>Editar</th>
                        <th>Excluir</th>
                    </tr>
                    </thead>

                    <tbody></tbody>

                </table>
            </div>
        </div>

        <div class="box-footer">
            <a href="{!! url('admin/coordenadores/novo') !!}" class="btn btn-success">Novo Coordenador</a>
        </div>
    </div>

    <div class="modal modal-danger fade" id="modal-danger">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">PERIGO</h4>
                </div>
                <div class="modal-body">
                    <p>Todos os dados desse coordenador serão removidos do sitema!</p>
                </div>
                <div class="modal-footer">
                    <form action="" method="POST" id="form-delete">
                        @method('DELETE')
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-outline">Confirmar</button>
                    </form>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

@section('js')
    <script>
        var url = '{!! url('admin/get-coordenadores') !!}';

        $(document).ready(function() {
            var table = $('#datatables').DataTable({
                'language'	  : {
                    'url' : 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
                },

                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'serverSide'  : true,
                'processing'  : true,

                'columns': [
                    {data: 'name'},
                    {data: 'cpf'},
                    {data: 'email'},
                    {data: 'phone'},
                    {data: 'edit', render: renderBtnEdit},
                    {data: 'delete', render: renderBtnDelete},
                ],

                'ajax': {
                    'url': url,
                    'type': 'GET',
                },

                'order': [[ 0, "asc" ]]
            });

            function renderBtnEdit(type, row, data){
                var url = '{!! url('admin/coordenadores/_id_/editar') !!}'.replace('_id_', data.id);
                return `<a href='${url}' class='btn btn-primary'><i class='fa fa-edit'></i></a>`;
            }

            function renderBtnDelete(type, row, data){
                return `<button class='btn btn-danger btn-delete' onclick='modalDelete("${data.id}")'><i class='fa fa-trash'></i></button>`;
            }
        });

        function modalDelete(id){
            var action = '{!! route('coordenadores.delete', request()->school) !!}';
            $('#id').val(id);
            $("#form-delete").attr("action", action);
            $('#modal-danger').modal('show');
        }
    </script>
@stop

@stop