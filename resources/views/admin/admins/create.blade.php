@extends('adminlte::page')

@section('title', 'Escolas')

@section('content_header')
<h1>Cadastrar SuperAdmin</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Super Admin</a></li>
	<li><a href="#">Cadastro</a></li>
</ol>
@stop

@section('content')

<div class="box">

	<div class="box-body">
		@include('admin.includes.alerts')

		<form action="{!! route('admin.store', request()->school) !!}" method="POST" class="form">
			@csrf
			<br>
			<div class="form-group">
				<label class="col-sm-2">Nome</label>
				<div class="col-sm-10">
					<input type="text" name="name" value="{!! old('name') !!}" class="form-control" placeholder="Nome do administrador">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-2">E-mail</label>
				<div class="col-sm-10">
					<input type="email" name="email" value="{!! old('email') !!}" class="form-control" placeholder="E-Mail do administrador">
				</div>
			</div>

			<br><br>
			<div class="form-group">
				<label class="col-sm-2">Telefone</label>
				<div class="col-sm-10">
					<input type="text" name="phone" value="{!! old('phone') !!}" class="form-control phone_with_ddd" placeholder="Telefone do administrador">
				</div>
			</div>

			<div class="box-footer">
				<button class="btn btn-success" type="button" onclick="history.go(-1)">Voltar</button>
				<button class="btn btn-primary">Salvar</button>
			</div>
		</form>

	</div>
</div>


@stop