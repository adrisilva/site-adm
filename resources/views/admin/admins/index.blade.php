@extends('adminlte::page')

@section('title', 'Administradores')

@section('content_header')
<h1>Administradores</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
	<li><a href="#">Admistradores</a></li>
</ol>
@stop

@section('content')

<div class="box box-primary">

	<div class="box-body">
		@include('admin.includes.alerts')

		<div class="table-responsive">

			<table class="table" id="datatables">
				<thead>
					<tr>
						<th>Nome</th>
						<th>E-Mail</th>
					</tr>
				</thead>

				<tbody></tbody>

			</table>
		</div>
	</div>

	<div class="box-footer">
		<a href="{!! route('admin.create', request()->school) !!}" class="btn btn-success">Novo Super Admin</a>
	</div>
</div>

@section('js')
    <script>
    	$(document).ready(function() {
	    	var table = $('#datatables').DataTable({
	    		'language'	  : {
	    			'url' : 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
	    		},

	    		'paging'      : true,
	    		'lengthChange': false,
	    		'searching'   : true,
	    		'ordering'    : true,
	    		'info'        : false,
	    		'autoWidth'   : false,
	    		'serverSide'  : true,
	    		'processing'  : true,
	    		
	    		'columns': [
		    		{data: "name"},
		    	 	{data: 'email'}
	    		],

	    		'ajax': {
	    			'url': '{!! route('admin.get', request()->school) !!}',
	    			'type': 'GET',
	    		},

                'order': [[ 0, "asc" ]]
	    	});
	    })
	</script>
@stop

@stop