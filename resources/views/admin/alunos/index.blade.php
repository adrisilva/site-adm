@extends('adminlte::page')

@section('title', 'Alunos')

@section('content_header')
	<h1>Alunos</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
		<li><a href="#">Alunos</a></li>
	</ol>
@stop

@section('content')

	<div class="box box-primary">

		<div class="box-body">
			@if(session()->has('success'))
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert">×</button>
					{!! session()->get('success') !!}
				</div>
			@endif

			<div class="table-responsive">

				<table class="table" id="datatables">
					<thead>
					<tr>
						<th>Matrícula</th>
						<th>Nome</th>
						<th>E-Mail</th>
						<th>Curso</th>
						<th>Série</th>
						<th>Editar</th>
						<th>Excluir</th>
					</tr>
					</thead>

					<tbody></tbody>

				</table>
			</div>
		</div>

		<div class="box-footer">
			<a href="{!! url('admin/alunos/novo') !!}" class="btn btn-success">Novo Aluno</a>
		</div>
	</div>

	<div class="modal modal-danger fade" id="modal-danger">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">PERIGO</h4>
				</div>
				<div class="modal-body">
					<p>Todos os dados desse aluno serão removidos do sitema!</p>
				</div>
				<div class="modal-footer">
					<form action="" method="POST" id="form-delete">
						@method('DELETE')
						@csrf
						<input type="hidden" name="email_aluno" id="email_aluno">
						<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cancelar</button>
						<button type="submit" class="btn btn-outline">Confirmar</button>
					</form>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

@section('js')
	<script>
        var url = '{!! url('admin/get-alunos') !!}';

        $(document).ready(function() {
            var table = $('#datatables').DataTable({
                'language'	  : {
                    'url' : 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json',
                },

                'paging'      : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'serverSide'  : true,
                'processing'  : true,

                'columns': [
                    {data: "registration"},
                    {data: 'name'},
                    {data: 'email'},
                    {data: 'course'},
                    {data: 'series'},
					{data: 'edit', render: renderBtnEdit},
					{data: 'delete', render: renderBtnDelete},
                ],

                'ajax': {
                    'url': url,
                    'type': 'GET',
                },

                'order': [[ 1, "asc" ]]
            });

            function renderBtnEdit(type, row, data){
                var url = '{!! url('admin/alunos/_id_/editar') !!}'.replace('_id_', data.id);
				return `<a href='${url}' class='btn btn-primary'><i class='fa fa-edit'></i></a>`;
            }

            function renderBtnDelete(type, row, data){
                return `<button class='btn btn-danger btn-delete' onclick='modalDelete("${data.email}")'><i class='fa fa-trash'></i></button>`;
            }
        });

        function modalDelete(email_aluno){
            var action = '{!! route('alunos.delete', request()->school) !!}';
            $('#email_aluno').val(email_aluno);
            $("#form-delete").attr("action", action);
            $('#modal-danger').modal('show');
        }
	</script>
@stop

@stop