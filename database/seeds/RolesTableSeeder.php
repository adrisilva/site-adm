<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_adm = Role::create(['name' => 'admin']);
        $permission_adm = Permission::create(['name' => 'Acesso ao projeto da sua respectiva instituição']);
    }
}
