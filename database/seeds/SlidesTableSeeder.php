<?php

use App\Models\Slide;
use Illuminate\Database\Seeder;

class SlidesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Slide::create([
        	'media_id'	=> '',
        	'title' 	=> 'Alimento da Mente',
        	'subtitle'	=> 'Aqui formamos mais que profissionais!'
        ]);

        Slide::create([
        	'media_id'	=> '',
        	'title' 	=> 'Alimento da Mente',
        	'subtitle'	=> 'Cada dia um degrau, cada degrau uma luta e a cada luta um aprendizado.'
        ]);

        Slide::create([
        	'media_id'	=> '',
        	'title' 	=> 'Alimento da Mente na sua escola',
        	'subtitle'	=> 'Descubra já o que é o projeto Alimento da Mente e leve ele para sua instituição de educação, sempre uma versão com a cara da sua escola, sempre com novas atualizações, sempre seguro e sempre ajudando no aprendizado do aluno.'
        ]);
    }
}
