<?php

use Illuminate\Database\Seeder;

class InsertRoleDiretor extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_adm = \Spatie\Permission\Models\Role::create(['name' => 'diretor']);
        $permission_adm = \Spatie\Permission\Models\Permission::create(['name' => 'Diretor']);
    }
}
