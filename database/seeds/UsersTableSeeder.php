<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ROLE DE SUPER ADMIN
        $email_super = 'superadmin@email.com';

        if(!DB::table('users')->where('email', $email_super)->first()){

            $role = Role::create(['name' => 'super_admin']);
            $permission = Permission::create(['name' => 'Acesso total ao projeto ADM']);
            $role->givePermissionTo($permission);
            $permission->assignRole($role);

            $super_admin = User::create([
                'name'      => 'Super Admin',
                'email'     => $email_super,
                'password'  => bcrypt('admin'),
                'phone'     => '4002-8922'
            ]);

            $super_admin->assignRole('super_admin');
        }
 
    }
}
