<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$about = "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque nisi nisi, porttitor eu tempor malesuada, tristique at nibh. Aliquam erat volutpat. Nulla in nunc eget nulla dignissim placerat ut et leo. Integer viverra aliquam est, ac iaculis massa euismod id. Aenean sit amet mi sollicitudin, laoreet dui sed, consectetur tellus. Vestibulum rutrum dictum lacus in vehicula. Curabitur vel vulputate nulla. Maecenas a hendrerit dolor. Nullam in elit sapien. Maecenas vel nulla tortor.</p>

			<p>Proin in sapien cursus, facilisis justo eu, gravida odio. Praesent vitae nisi varius, aliquet est et, ullamcorper nisl. Suspendisse mauris urna, posuere quis pulvinar sed, porta nec tortor. Curabitur interdum metus dolor. Integer non efficitur nibh, maximus elementum ipsum. Etiam tempus rutrum nisi vel vulputate. Etiam porta tortor dui, nec rutrum nibh hendrerit vel. Maecenas ut ante hendrerit nibh vulputate rhoncus sed eu massa. Nulla ac semper magna. Etiam placerat, augue ut consequat porttitor, nulla nunc feugiat nibh, et porta dolor nibh vitae lorem. Etiam faucibus tellus vitae urna placerat, quis scelerisque orci egestas. Morbi eu commodo risus. Praesent maximus eros eget iaculis mattis. Pellentesque rutrum maximus nisl, eget tristique erat tempor ac. Curabitur eu arcu elit. Fusce commodo massa quis felis placerat facilisis.</p>

			<p>Donec eu luctus dolor, et pulvinar purus. Suspendisse volutpat nulla vitae aliquet placerat. Cras porttitor condimentum elit vitae varius. Aliquam ac ipsum non ex pretium imperdiet. Aenean fringilla metus sit amet ex consequat, vel congue ex molestie. Aliquam non laoreet orci. Donec tempus placerat dolor et hendrerit. In sodales mauris id diam iaculis aliquam vehicula non sem. Ut iaculis iaculis tortor non porta. Praesent ultrices tincidunt sem. Nunc erat nulla, iaculis malesuada odio in, mollis blandit dolor. Sed vestibulum felis vel elementum faucibus. In eleifend nunc sit amet sapien viverra, at scelerisque mauris elementum. Curabitur ut felis metus.</p>";

        Content::create([
        	'title' 	=> 'EEEP',
        	'about'		=> $about
        ]);
    }
}
