<?php

Route::get('first-access', 'Admin\UserController@firstAccess')->name('user.first_access');
Route::put('first-access', 'Admin\UserController@changePassword')->name('user.change_password');
Route::post('logout', 'Admin\Auth\LoginController@logout');

Route::group(['domain' => '{school}.'.env('MAIN_DOMAIN')], function(){

	Route::get('', 'SiteController@showSchool')->name('school.index');

	Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){

		Route::get('', 'Auth\LoginController@showLoginForm');
		Route::post('', 'Auth\LoginController@login')->name('admin.login');
		Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');

		Route::group(['middleware' => ['first_access', 'role:diretor|super_admin|admin']], function(){

			Route::group(['namespace' => 'CMS'], function(){
				Route::get('edit/home', 'HomeCmsController@edit')->name('cms.home.edit');
				Route::put('edit/home', 'HomeCmsController@update')->name('cms.home.update');

				Route::get('images', 'ImageCmsController@index')->name('cms.image.index');
				Route::post('images/{id}/change-status', 'ImageCmsController@changeStatus')->name('cms.image.change.status');
				Route::get('images/{id}/edit', 'ImageCmsController@edit')->name('cms.image.edit');
				Route::put('images/{id}/edit', 'ImageCmsController@update')->name('cms.image.update');

				Route::get('slides', 'SlideCmsController@index')->name('cms.slide.index');
				Route::post('slides/{id}/change-status', 'SlideCmsController@changeStatus')->name('cms.slide.change.status');
				Route::get('slides/{id}/edit', 'SlideCmsController@edit')->name('cms.slide.edit');
				Route::put('slides/{id}/edit', 'SlideCmsController@update')->name('cms.slide.update');
			});

			Route::get('alunos', 'AlunoController@index')->name('alunos.index');
			Route::get('alunos/novo', 'AlunoController@create')->name('alunos.create');
			Route::post('alunos/novo', 'AlunoController@store')->name('alunos.store');
			Route::get('get-alunos', 'AlunoController@getAlunos')->name('alunos.get');
			Route::delete('alunos/delete', 'AlunoController@delete')->name('alunos.delete');
            Route::get('alunos/{id}/editar', 'AlunoController@edit')->name('alunos.edit');
            Route::put('alunos/{id}/editar', 'AlunoController@update')->name('alunos.update');

			Route::get('professores', 'ProfessorController@index')->name('professores.index');
			Route::get('professores/novo', 'ProfessorController@create')->name('professores.create');
			Route::post('professores/novo', 'ProfessorController@store')->name('professores.store');
			Route::get('get-professores', 'ProfessorController@getProfessores')->name('professores.get');
            Route::delete('professores/delete', 'ProfessorController@delete')->name('professores.delete');
            Route::get('professores/{id}/editar', 'ProfessorController@edit')->name('professores.edit');
            Route::put('professores/{id}/editar', 'ProfessorController@update')->name('professores.update');

			Route::get('coordenadores', 'CoordenadorController@index')->name('coordenadores.index');
			Route::get('coordenadores/novo', 'CoordenadorController@create')->name('coordenadores.create');
			Route::post('coordenadores/novo', 'CoordenadorController@store')->name('coordenadores.store');
			Route::get('get-coordenadores', 'CoordenadorController@getCoordenadores')->name('coordenadores.get');
            Route::delete('coordenadores/delete', 'CoordenadorController@delete')->name('coordenadores.delete');
            Route::get('coordenadores/{id}/editar', 'CoordenadorController@edit')->name('coordenadores.edit');
            Route::put('coordenadores/{id}/editar', 'CoordenadorController@update')->name('coordenadores.update');
		});
	});

	Route::group(['prefix' => '{school}'], function(){
		Route::get('', 'SiteController@showSchool');
	});
});

//ROTAS SUPER ADMIN
Route::get('admin/dashboard', 'Admin\HomeController@index')->middleware('first_access')->name('home');
Route::get('', 'SiteController@index')->name('index');
Route::get('contato', 'SiteController@contact')->name('contact');
Route::post('email-contato', 'SiteController@sendEmailContact');

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function(){
	
	Route::get('', 'Auth\LoginController@showLoginForm');
	Route::post('', 'Auth\LoginController@login')->name('admin.login');

    Route::group(['middleware' => ['first_access', 'role:super_admin']], function(){

       // Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');
		Route::get('escolas', 'SchoolController@index')->name('school.index');
		Route::get('get-escolas', 'SchoolController@getSchools')->name('school.get');
		Route::get('escolas/cadastro', 'SchoolController@create')->name('school.create');
		Route::post('escolas/cadastro', 'SchoolController@store')->name('school.store');
        Route::get('editar/{slug}/escola', 'SchoolController@edit')->name('school.edit');
        Route::put('editar/{slug}/escola', 'SchoolController@update')->name('school.update');
        Route::delete('delete/escola/{slug}', 'SchoolController@delete')->name('school.delete');

        Route::get('admins', 'AdminController@index')->name('admin.index');
        Route::get('get-super-admins', 'AdminController@getSuperAdmins')->name('admin.get');
        Route::get('admins/cadastro', 'AdminController@create')->name('admin.create');
        Route::post('admins/cadastro', 'AdminController@store')->name('admin.store');

        Route::get('diretores', 'DirectorController@index')->name('director.index');
        Route::get('get-directors', 'DirectorController@getDirectors')->name('director.get');

        Route::get('first_access', 'SchoolController@firstAccessCount')->name('school.first_access');
        Route::get('first-access-count', 'SchoolController@countFirstAccessUsers')->name('school.first_access.count');
    });
});