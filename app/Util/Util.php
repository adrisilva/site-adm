<?php 

namespace App\Util;

class Util
{
    public static function generatePassword()
	{
		$password = null;
		for($i = 1; $i <= 6; $i++){
			$num = rand(1, 9);
			$password .= $num;
		}
		return $password;
	}
}