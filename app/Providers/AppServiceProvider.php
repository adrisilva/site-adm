<?php

namespace App\Providers;

use App\Models\Image;
use App\Models\Slide;
use App\Models\Content;
use App\Observers\ImageObserver;
use App\Observers\SlideObserver;
use App\Observers\ContentObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
       // Image::observe(ImageObserver::class);
       // Content::observe(ContentObserver::class);
      //  Slide::observe(SlideObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
