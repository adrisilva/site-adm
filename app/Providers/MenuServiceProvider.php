<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $is_school = request()->getHttpHost() != env('MAIN_DOMAIN');
        $events->listen(BuildingMenu::class, function (BuildingMenu $event) use($is_school) {

            if(Auth::user()->hasRole('super_admin') && $is_school){

                $event->menu->add('ESCOLA');

                $event->menu->add([
                        'text' => 'Alunos',
                        'url'  => 'admin/alunos',
                        'icon' => 'group'
                ]);

                $event->menu->add([
                    'text' => 'Professores',
                    'url'  => 'admin/professores',
                    'icon' => 'graduation-cap'
                ]);

                $event->menu->add([
                    'text' => 'Coordenadores',
                    'url'  => 'admin/coordenadores',
                    'icon' => 'lock',
                ]);

                $event->menu->add([
                    'text' => 'Textos da Home',
                    'url'  => 'admin/edit/home',
                    'icon' => 'home'
                ]);

                $event->menu->add([
                    'text' => 'Imagens',
                    'url'  => 'admin/images',
                    'icon' => 'picture-o'
                ]);

                $event->menu->add([
                    'text' => 'Slides',
                    'url'  => 'admin/slides',
                    'icon' => 'sliders'
                ]);

            }
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
