<?php

namespace App\Services;

use App\Mail\CreateSchoolMail;
use App\Util\Util;
use Illuminate\Http\Request;
use App\Models\Conectados\Admin;
use App\Models\Conectados\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class AdminService
{
    public static $connections = [
        'mysql_leitura',
        'mysql_linguagem',
        'mysql_matematica'
    ];

    public static function getAdminLeitura($slug)
    {
        $school = New School;
        $school->setConnection('mysql_leitura');
        $school = $school->where('slug', $slug)->first();
        

        $admin = new Admin;
        $admin->setConnection('mysql_leitura');
        $admin = $admin->where('school_id', $school->id)->first();

        return $admin;
    }

    public static function getAdminLinguagem($slug)
    {
        $school = New School;
        $school->setConnection('mysql_linguagem');
        $school = $school->where('slug', $slug)->first();
        

        $admin = new Admin;
        $admin->setConnection('mysql_linguagem');
        $admin = $admin->where('school_id', $school->id)->first();

        return $admin;
    }

    public static function getAdminMatematica($slug)
    {
        $school = New School;
        $school->setConnection('mysql_matematica');
        $school = $school->where('slug', $slug)->first();
        

        $admin = new Admin;
        $admin->setConnection('mysql_matematica');
        $admin = $admin->where('school_id', $school->id)->first();

        return $admin;
    }

    public static function updateAdmins($slug, Request $request)
    {
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $school = new School;
            $school->setConnection(self::$connections[$i]);
            $school = $school->where('slug', $slug)->first();

            $admin = new Admin;
            $admin->setConnection(self::$connections[$i]);

            $password = Util::generatePassword();
            $adminEmail = $admin->where('email', $request->coordenador_email[$i])->first();

            if(empty($adminEmail)){
                Mail::to($request->coordenador_email[$i])->send(new CreateSchoolMail($request->coordenador_name[$i],  $request->coordenador_email[$i], $password));
                $data =  [
                    'name'      => $request->coordenador_name[$i],
                    'email'     => $request->coordenador_email[$i],
                    'phone'     => $request->coordenador_telefone[$i],
                    'password'  => bcrypt($password),
                    'cpf'       => $request->coordenador_cpf[$i],
                ];
            }else{
                $data = [
                    'name'  => $request->coordenador_name[$i],
                    'phone' => $request->coordenador_telefone[$i],
                    'cpf'   => $request->coordenador_cpf[$i],
                ];
            }

            $admin->find($request->coordenador_id[$i])->update($data);

            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function storeSuperAdmin($request)
    {
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $admin = new Admin;
            $admin->setConnection(self::$connections[$i]);

            if($request->school_id != null){
                $data = [
                    'name'  => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => $request->password,
                    'school_id' => $request->school_id
                ];
            }else{
                $data = [
                    'name'  => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => bcrypt('superadmin')
                ];
            }

            $admin->create($data);

            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function updateCoodenador($slug, $request)
    {
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $school = new School;
            $school->setConnection(self::$connections[$i]);
            $school = $school->where('slug', $slug)->first();

            $admin = new Admin;
            $admin->setConnection(self::$connections[$i]);

            $password = Util::generatePassword();
            $adminEmail = $admin->where('email', $request->coordenador_email[$i])->first();

            if(empty($adminEmail)){
                Mail::to($request->coordenador_email[$i])->send(new CreateSchoolMail($request->coordenador_name[$i],  $request->coordenador_email[$i], $password));
                $data =  [
                    'name'  => $request->name,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'password' => bcrypt($password),
                ];
            }else{
                $data = [
                    'name'  => $request->name,
                    'phone' => $request->phone,
                ];
            }

            $admin->where('school_id', $school->id)->update($data);

            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function storeDiretor($slug, $request)
    {
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $school = new School;
            $school->setConnection(self::$connections[$i]);
            $school = $school->where('slug', $slug)->first();

            $admin = new Admin;
            $admin->setConnection(self::$connections[$i]);

            $data = [
                'name'       => $request->diretor_name,
                'email'      => $request->email,
                'phone'      => $request->diretor_tel,
                'cpf'        =>  $request->cpf,
                'password'   => $request->password,
                'school_id'  => $school->id,
            ];

            $admin->create($data);

            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function updateDiretor($slug, $request)
    {
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $school = new School;
            $school->setConnection(self::$connections[$i]);
            $school = $school->where('slug', $slug)->first();

            $admin = new Admin;
            $admin->setConnection(self::$connections[$i]);

            $adminEmail = $admin->where('email', $request->email)->first();
            if(empty($adminEmail)){
                Mail::to($request->email)->send(new CreateSchoolMail($request->diretor_name,  $request->email, $request->password));
                $data =  [
                    'name'          => $request->diretor_name,
                    'email'         => $request->email,
                    'phone'         => $request->diretor_tel,
                    'cpf'           => $request->cpf,
                    'password'      => $request->password,
                ];
            }else{
                $data = [
                    'name'          => $request->diretor_name,
                    'phone'         => $request->diretor_tel,
                    'cpf'           =>  $request->cpf,
                ];
            }

            $admin->where('id', '!=', $request->coordenador_id[$i])->where('school_id', $school->id)->update($data);

            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }
}