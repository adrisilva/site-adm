<?php 

namespace App\Services;

use App\Models\Conectados\School;
use Exception;
use Illuminate\Support\Collection;
use Validator;
use App\Util\Util;
use Illuminate\Http\Request;
use App\Mail\CreateSchoolMail;
use App\Models\Conectados\User;
use App\Models\Conectados\Admin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\QueryException;

class UserService
{
    public static $connections = [
        'mysql_leitura',
        'mysql_linguagem',
        'mysql_matematica'
    ];

    public static function store($request, $type)
    {
        $user = new User;
        $school = new School;
            
        $password = Util::generatePassword();

        $request->merge([
            'type'  => $type,
            'password' => bcrypt($password)
        ]);
        
        for($i = 0; $i < 3; $i++){

            $validator = self::validateData($request, self::$connections[$i], $type);

            if($validator !== null){
                $data['success'] = false;
                $data['data'] = $validator;
                return $data;
            }

            $school->setConnection(self::$connections[$i]);
            $school = $school->where('slug', request()->school)->first();

            $request->merge([
                'school_id' => $school->id
            ]);

            DB::connection(self::$connections[$i])->beginTransaction();

            $user->setConnection(self::$connections[$i]);  
            $user = $user->create($request->all());

            DB::connection(self::$connections[$i])->commit();
        }

        Mail::to($user)->send(new CreateSchoolMail($user->name, $user->email, $password));

        $data['success'] = true;
        $data['data'] = null;
        return $data;
    }

    public static function validateData($request, $connection, $type)
    {
        if($type == 'Aluno'){
            $rules = [
                'email'         => 'required|unique:'.$connection.'.users,email',
                'registration'  => 'required|unique:'.$connection.'.users,registration',
                'birth'         => 'required',
                'series'        => 'required',
                'shift'         => 'required',
            ];
        }else{
            $rules = [
                'email'         => 'required|unique:'.$connection.'.users,email',
                'cpf'           => 'required|unique:'.$connection.'.users,cpf',
                'birth'         => 'required',
                'matter'        => 'required'
            ];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return $validator;
        }
    }

    public static function validateDataUpdate($request, $connection, $type, $user)
    {
        if($type == 'Aluno'){
            $rules = [
                'name'          => 'required',
                'email'         => 'required|unique:'.$connection.'.users,email,'.$user->id,
                'registration'  => 'required|unique:'.$connection.'.users,registration,'.$user->id,
                'birth'         => 'required',
                'series'        => 'required',
                'shift'         => 'required',
            ];
        }else{
            $rules = [
                'name'          => 'required',
                'email'         => 'required|unique:'.$connection.'.users,email,'.$user->id,
                'cpf'           => 'required|unique:'.$connection.'.users,cpf,'.$user->id,
                'birth'         => 'required',
                'matter'        => 'required'
            ];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()){
            return $validator;
        }
    }

    public static function delete($email)
    {
        $user = new User;

        for($i = 0; $i < 3; $i++){

            DB::connection(self::$connections[$i])->beginTransaction();
            $user->setConnection(self::$connections[$i]);

            $aluno = $user->where('email', $email)->delete();

            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function update($request, $oldEmail, $type)
    {
        $user = new User;
        $password = Util::generatePassword();

        for($i = 0; $i < 3; $i++){

            DB::connection(self::$connections[$i])->beginTransaction();

            $user->setConnection(self::$connections[$i]);

            $userFind = $user->where('email', $oldEmail)->first();

            $validator = self::validateDataUpdate($request, self::$connections[$i], $type, $userFind);

            if($validator !== null){
                $data['success'] = false;
                $data['data'] = $validator;
                return $data;
            }

            $dataUser = $request->except('password');

            if($request->has('password') && $request->password != null){
                $dataUser['password'] = bcrypt($request->password);
            }

            if($dataUser['email'] != $userFind->email){
                $dataUser['password'] = bcrypt($password);
                $dataUser['first_access'] = 1;
                Mail::to($dataUser['email'])->send(new CreateSchoolMail($dataUser['name'], $dataUser['email'], $password));
            }

            $userUpdate = $userFind->update($dataUser);

            DB::connection(self::$connections[$i])->commit();
        }

        $data['success'] = true;
        $data['data'] = null;
        return $data;
    }

    public static function countFirstAccess()
    {
        $school = new School();
        $user   = new User;

        $users_count = [];

        foreach(self::$connections as $connection){
            $school->setConnection($connection);
            $schools = $school->all();
            foreach($schools as $school){
                $user->setConnection($connection);

                $usersTotal = $user->where('school_id', $school->id)->where('type', 'Aluno')->count();
                $users = $user->where('school_id', $school->id)->where('type', 'Aluno')->where('first_access', 1)->count();

                $users_count[$school->slug][$connection] = $users;
                $users_count[$school->slug]["school"] = $school->slug;
                $users_count[$school->slug]['total_alunos'] = $usersTotal;
            }

        }

        return $users_count;
    }
}