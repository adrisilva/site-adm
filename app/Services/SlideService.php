<?php

namespace App\Services;

use App\Models\Media;

class SlideService
{

    public static function createSlides($school_id)
    {
       //media slide
       Media::create([
        'path' 		=> 'medias/images/',
        'filename'	=> 'carousel1.jpg',
        'extension'	=> 'jpg',
        'type'		=> 'image'
        ])->slides()
        ->create([
            'title' 	=> 'Alimento da Mente',
            'subtitle'	=> 'Aqui formamos mais que profissionais!',
            'school_id' => $school_id
        ]);

        //media slide
        Media::create([
            'path' 		=> 'medias/images/',
            'filename'	=> 'carousel2.jpg',
            'extension'	=> 'jpg',
            'type'		=> 'image'
        ])->slides()
            ->create([
                'title' 	=> 'Alimento da Mente',
                'subtitle'	=> 'Cada dia um degrau, cada degrau uma luta e a cada luta um aprendizado.',
                'school_id' => $school_id
            ]);

        //media slide
        Media::create([
            'path' 		=> 'medias/images/',
            'filename'	=> 'carousel3.jpg',
            'extension'	=> 'jpg',
            'type'		=> 'image'
        ])->slides()
            ->create([
                'title' 	=> 'Alimento da Mente na sua escola',
                'subtitle'	=> 'Descubra já o que é o projeto Alimento da Mente e leve ele para sua instituição de educação, sempre uma versão com a cara da sua escola, sempre com novas atualizações, sempre seguro e sempre ajudando no aprendizado do aluno.',
                'school_id' => $school_id
            ]);
    }

}