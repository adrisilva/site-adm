<?php

namespace App\Services;

use App\Models\Media;

class ImageService
{
    public static function createImages($school_id)
    {
        Media::create([
            'path' 		=> 'medias/images/',
            'filename'	=> 'foot1.jpg',
            'extension'	=> 'jpg',
            'type'		=> 'image'
            ])->images()
            ->create([
                'title' 	=> 'EEEP. Professor Onélio Porto Avenida E, 471 - Prefeito José Walter - Fortaleza-CE',
                'subtitle'	=> 'Escola Estadual de Educação Profissional Professor Onélio Porto',
                'school_id' => $school_id
            ]);

        Media::create([
            'path' 		=> 'medias/images/',
            'filename'	=> 'foot2.jpg',
            'extension'	=> 'jpg',
            'type'		=> 'image'
            ])->images()
            ->create([
                'title' 	=> 'Professores coordenadores do ADM Onélio Porto',
                'subtitle'	=> 'Da direita para esquerda: Karoline Holanda (Professora de Literatura e redação, coordenadora do ADM Onélio Porto e administradora do Conectados LC), Alexandre Nicholas (Professor de Geometria e administrador do Conectados MT Onélio Porto), Marisa Honorato (Bibliotecária e administradora do Conectados LT Onélio Porto).',
                'school_id' => $school_id
            ]);
    }
}