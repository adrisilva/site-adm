<?php 

namespace App\Services;

use App\Models\Conectados\SystemConfig;
use Validator;
use Exception;
use App\Util\Util;
use Illuminate\Http\Request;
use App\Mail\CreateSchoolMail;
use App\Models\Conectados\Admin;
use App\Models\Conectados\School;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\QueryException;

class SchoolService
{
    public static $connections = [
        'mysql_leitura',
        'mysql_linguagem',
        'mysql_matematica'
    ];

    public static function createSchools($request, $diretor, $password_diretor)
    {
        $school = new School;
            
        for($i = 0; $i < 3; $i++){

            $validator = self::validateData($request, self::$connections[$i]);

            if($validator !== null){
                $data['success'] = false;
                $data['data'] = $validator;
                return $data;
            }

            DB::connection(self::$connections[$i])->beginTransaction();

            $school->setConnection(self::$connections[$i]);
            $school = $school->create([
                'slug' => $request->slug,
                'name'  => $request->name
            ]);

            if(self::$connections[$i] == 'mysql_linguagem'){
                SystemConfig::create([
                    'name' 	=> 'issues_released',
                    'label' => 'Liberar questões da olímpiada',
                    'school_id' => $school->id
                ]);
            }
            
            $password = Util::generatePassword();
            $admin = new Admin;
            $admin->setConnection(self::$connections[$i]);
            $admin = $admin->create([
                'name'      => $request->coordenador_name[$i],
                'email'     => $request->coordenador_email[$i],
                'phone'     => $request->coordenador_telefone[$i],
                'cpf'       => $request->coordenador_cpf[$i],
                'password'  => bcrypt($password),
                'school_id' => $school->id
            ]);

            $diretor = $admin->create([
                'name'      => $diretor->name,
                'email'     => $diretor->email,
                'phone'     => $diretor->phone,
                'cpf'       => $diretor->cpf,
                'password'  => bcrypt($password_diretor),
                'school_id' => $school->id
            ]);

            Mail::to($admin)->send(new CreateSchoolMail($admin->name, $admin->email, $password));
        }

        DB::connection(self::$connections[0])->commit();
        DB::connection(self::$connections[1])->commit();
        DB::connection(self::$connections[2])->commit();

        $data['success'] = true;
        $data['data'] = null;
        return $data;
    }

    public static function updateSchools($slug, Request $request)
    {
        $school = new School;
              
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $school->setConnection(self::$connections[$i]);
            $school->where('slug', $slug)->update([
                'slug' => $request->slug,
                'name'  => $request->name
            ]);;
  
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function deleteSchools($slug)
    {
        $school = new School;
              
        for($i = 0; $i < 3; $i++){
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();
            DB::connection(self::$connections[$i])->beginTransaction();

            $school->setConnection(self::$connections[$i]);
            $school->where('slug', $slug)->delete();

            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
            DB::connection(self::$connections[$i])->commit();
        }
    }

    public static function validateData($request, $connection)
    {
        $rules = [
            'coordenador_name.*'        => 'required',
            'coordenador_email.*'       => 'required|unique:'.$connection.'.admins,email',
            'coordenador_telefone.*'    => 'required|unique:'.$connection.'.admins,phone',
            'coordenador_cpf.*'    => 'required|unique:'.$connection.'.admins,phone',
        ];

        $messages = [
            'coordenador_name.*.required' => 'O nome do coordenador do projeto é obrigatório.',
            
            'coordenador_email.*.required' => 'O email do coordenador do projeto é obrigatório.',
            'coordenador_email.*.unique' => 'O e-mail de um dos coordenadores já está em uso.',

            'coordenador_telefone.*.required' => 'O telefone do coordenador do projeto é obrigatório.',
            'coordenador_telefone.*.unique' => 'O telefone de um dos coordenadores já está em uso.',

            'coordenador_cpf.*.required' => 'O CPF do coordenador do projeto é obrigatório.',
            'coordenador_cpf.*.unique' => 'O CPF de um dos coordenadores já está em uso.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()){
            return $validator;
        }
    }
}