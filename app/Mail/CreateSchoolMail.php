<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSchoolMail extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $email;
    public $password;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user_name, $user_email, $user_password)
    {
        $this->name        = $user_name;
        $this->email       = $user_email;
        $this->password    = $user_password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('contato@alimentodamente.com.br')
                ->subject('Alimento da Mente')
                ->view('admin.email.cadastro');
    }
}
