<?php
/**
 * Created by PhpStorm.
 * User: Silva
 * Date: 01/11/2018
 * Time: 22:41
 */
namespace App\Scopes;

use App\Models\Conectados\School;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Database\Eloquent\Builder;

class SchoolLocalScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        $school = School::where('slug', request()->school)->first();
        $builder->where('school_id', $school->id);
    }
}