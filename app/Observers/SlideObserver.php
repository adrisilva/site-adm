<?php

namespace App\Observers;

use App\Models\Slide;
use App\Models\School;

class SlideObserver
{
    private $school;

    public function __construct()
    {
        $this->school = School::where('slug', request()->school)->first();
    }
    /**
     * Handle the slide "created" event.
     *
     * @param  \App\Models\Slide  $slide
     * @return void
     */
    public function created(Slide $slide)
    {
        //
    }

    /**
     * Handle the slide "updated" event.
     *
     * @param  \App\Models\Slide  $slide
     * @return void
     */
    public function updated(Slide $slide)
    {
        //
    }

    /**
     * Handle the slide "deleted" event.
     *
     * @param  \App\Models\Slide  $slide
     * @return void
     */
    public function deleted(Slide $slide)
    {
        //
    }

    /**
     * Handle the slide "restored" event.
     *
     * @param  \App\Models\Slide  $slide
     * @return void
     */
    public function restored(Slide $slide)
    {
        //
    }

    /**
     * Handle the slide "force deleted" event.
     *
     * @param  \App\Models\Slide  $slide
     * @return void
     */
    public function forceDeleted(Slide $slide)
    {
        //
    }

}
