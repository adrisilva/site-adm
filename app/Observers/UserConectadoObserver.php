<?php

namespace App\Observers;

use App\Models\Conectados\User;

class UserConectadoObserver
{
    private $school;

    public function __construct()
    {
        $school = new School;
        $school->setConnection('mysql_leitura');
        $this->school = $school->where('slug', request()->school)->first();
    }
    /**
     * Handle the user "created" event.
     *
     * @param  \App\Models\Conectados\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the user "updated" event.
     *
     * @param  \App\Models\Conectados\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        //
    }

    /**
     * Handle the user "deleted" event.
     *
     * @param  \App\Models\Conectados\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the user "restored" event.
     *
     * @param  \App\Models\Conectados\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the user "force deleted" event.
     *
     * @param  \App\Models\Conectados\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }

    public function creating(User $user)
    {
        $user->school_id = $this->school->id;
    }
}
