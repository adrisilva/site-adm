<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Media extends Model
{
    use SoftDeletes;

    protected $table = 'medias';

	protected $fillable = [
		'path', 'filename', 'extension', 'type'
	];

	public function slides()
	{
		return $this->hasMany(Slide::class);
	}

	public function images()
	{
		return $this->hasMany(Image::class);
	}
}
