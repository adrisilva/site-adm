<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slide extends Model
{
    use SoftDeletes;

	protected $fillable = [
		'media_id', 'subtitle', 'title', 'active', 'school_id'
	];

	public function media()
	{
		return $this->belongsTo(Media::class, 'media_id', 'id');
	}

	public static $rules = [
		'title'	=> 'required',
		'subtitle'	=> 'required'
	];
}
