<?php

namespace App\Models;

use App\Scopes\SchoolLocalScope;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    public static $rules_change_password = [
        'password' => 'required|min:6|confirmed'
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'school_id', 'first_access', 'cpf'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules_super_admin = [
        'name'  => 'required',
        'email' => 'required|unique:users',
        'phone' => 'required|unique:users'
    ];

    public function school()
    {
        return $this->belongsTo(School::class);
    }
}
