<?php

namespace App\Models\Conectados;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_leitura';
    
    protected $table = 'users';
    
    public $timestamps = true;
	
   	protected $fillable = [
        'name', 'registration', 'birth', 'course', 'series', 'shift', 'type', 'matter',
        'email', 'password', 'school_id', 'cpf', 'first_access'
    ];
}
