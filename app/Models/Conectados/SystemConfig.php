<?php

namespace App\Models\Conectados;

use Illuminate\Database\Eloquent\Model;

class SystemConfig extends Model
{
    protected $connection = 'mysql_linguagem';

    protected $table = 'system_configs';

    public $timestamps = true;

    protected $fillable = [
       'label', 'name', 'status', 'school_id'
    ];
}
