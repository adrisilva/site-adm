<?php

namespace App\Models\Conectados;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $connection = 'mysql_leitura';

    protected $table = 'admins';
    
    public $timestamps = true;

    protected $fillable = ['name', 'email', 'password', 'active', 'phone', 'school_id', 'cpf'];

}
