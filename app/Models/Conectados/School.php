<?php

namespace App\Models\Conectados;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $connection = 'mysql_leitura';

    protected $table = 'schools';

    public $timestamps = true;

    protected $fillable = ['name', 'slug'];
}
