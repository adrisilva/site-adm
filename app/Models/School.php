<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
   	protected $fillable = [
        'name', 'inep', 'renewal_date', 'slug', 'lc_active', 'mt_active', 'lt_active'
    ];

    public static $rules = [
        'name'          => 'required|unique:schools',
        'slug'          => 'required|unique:schools',
        'inep'          => 'required|unique:schools',
        'diretor_name'              => 'required',
        'cpf'              => 'required',
        'diretor_email'             => 'required|unique:users,email',
        'diretor_tel'               => 'required|unique:users,phone',
        'cep'                       => 'required|min:9|max:9',
        'endereco'                  => 'required',
    ];

    public static $rulesUpdate = [
        'name'                      => 'required',
        'cpf'                      => 'required',
        'inep'                      => 'required',
        'diretor_name'              => 'required',
        'email'                     => 'required',
        'diretor_tel'               => 'required',
        'cep'                       => 'required',
        'endereco'                  => 'required',
    ];

    public function address()
    {
    	return $this->hasOne(Address::class);
    }

    public function content()
    {
    	return $this->hasOne(Content::class);
    }

    public function slides()
    {
        return $this->hasMany(Slide::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
