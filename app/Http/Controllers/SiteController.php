<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{
    public function index($school = null)
    {
        $schools = School::select('slug', 'name')->get();
        return view('index', compact('schools'));
    }

    public function contact()
    {
        $schools = School::select('slug', 'name')->get();
        return view('contact', compact('schools'));
    }

    public function showSchool($school)
    {
        $school = School::where('slug', $school)->first();
        $slides_count = $school->slides->where('active', 1);
        return view('site.index', compact('school', 'slides_count'));
    }

    public function sendEmailContact(Request $request)
    {
        Mail::send(new ContactMail($request->all()));
        return redirect('contato')->withSuccess('E-Mail Enviado com sucesso!');
    }
}
