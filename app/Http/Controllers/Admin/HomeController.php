<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(request()->school == null){
            $url = request()->url();
            $url_array = explode('/', $url);
            $url_array_two = explode('.', $url_array[2]);
            $school = $url_array_two[0];
        }

        return view('admin.dashboard', compact('school'));
    }
}
