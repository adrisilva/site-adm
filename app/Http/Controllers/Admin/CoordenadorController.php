<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CreateCoordenadorRequest;
use App\Http\Requests\UpdateCoordenadorRequest;
use App\Mail\CreateSchoolMail;
use App\Models\School;
use App\Models\User;
use App\Util\Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class CoordenadorController extends Controller
{
    public function index()
    {
        return view('admin.coordenador.index');
    }

    public function getCoordenadores()
    {
        $school = School::where('slug', request()->school)->first();
        $users = User::where('id', '!=', Auth::user()->id)
            ->where('school_id', $school->id)
            ->get();

        return datatables($users)->toJson();
    }

    public function create()
    {
        return view('admin.coordenador.create');
    }

    public function store(CreateCoordenadorRequest $request, $school)
    {
        $school = School::where('slug', $school)->first();
        $password = Util::generatePassword();
        $request->merge([
            'password'  => bcrypt($password),
            'school_id' => $school->id
        ]);
        $user = User::create($request->all())->assignRole('admin');
        Mail::to($user)->send(new CreateSchoolMail($user->name, $user->email, $password));
        return redirect()->back()->withSuccess('Coodenador cadastrado com sucesso');
    }

    public function edit($slug, $id)
    {
        $user = User::find($id);
        return view('admin.coordenador.edit', compact('user'));
    }

    public function update(UpdateCoordenadorRequest $request, $school, $id)
    {
        $user = User::find($id);

        $dataUser = $request->except('password');

        if($request->has('password') && $request->password != null){
            $dataUser['password'] = bcrypt($request->password);
        }

        $user->update($dataUser);

        return redirect()->back()->withSuccess('Dados atualizados com sucesso');
    }

    public function delete(Request $request)
    {
        $user = User::destroy($request->id);

        return redirect()->back()->withSuccess('Dados removidos com sucesso');
    }
}
