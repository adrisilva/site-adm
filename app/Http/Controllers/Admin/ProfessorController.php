<?php

namespace App\Http\Controllers\Admin;

use App\Services\UserService;
use Illuminate\Http\Request;
use App\Models\Conectados\User;
use App\Http\Controllers\Controller;

class ProfessorController extends Controller
{
    public static $matters = [
        'Algebra'           =>  'Algebra',
        'Artes'             =>  'Artes',
        'Base Técnica'      =>  'Base Técnica',
        'Biologia'          =>  'Biologia',
        'Educação Física'   =>  'Educação Física',
        'Física'            =>  'Física',
        'Geografia'         =>  'Geografia',
        'Geometria'         =>  'Geometria',
        'Gramática'         =>  'Gramática',
        'História'          =>  'História',
        'Literatura'        =>  'Literatura',
        'Química'           =>  'Química',
    ];

    public function index()
  	{	
  		return view('admin.professores.index'); 
  	}

  	public function getProfessores()
  	{
		$user = new User;
		$users = $user->where('type', 'Professor')->get();
		return datatables($users)->toJson();
  	}

  	public function create($school)
    {
        $matters = self::$matters;
        return view('admin.professores.create', compact('matters'));
    }

    public function store(Request $request, $school)
    {
        $user = UserService::store($request, 'Professor');

        if (!$user['success']) {
            return redirect()
                ->back()
                ->withErrors($user['data'])
                ->withInput();
        }

        return redirect()->back()->withSuccess('Professor cadastrado com sucesso');
    }

    public function delete(Request $request)
    {
        try{
            UserService::delete($request->email_professor);
            return redirect()->back()->withSuccess('Dados removidos com sucesso!');
        }catch (\Exception $ex){
            return redirect()->back()->withError('Erro ao remover os dados do aluno');
        }
    }

    public function edit($school, $id)
    {
        $user = User::find($id);
        $matters = self::$matters;
        return view('admin.professores.edit', compact('user', 'matters'));
    }

    public function update(Request $request, $school, $id)
    {
        $user = User::find($id);
        $user = UserService::update($request, $user->email, 'Professor');

        if (!$user['success']) {
            return redirect()
                ->back()
                ->withErrors($user['data'])
                ->withInput();
        }

        return redirect()->back()->withSuccess('Dados atualizados com sucesso');
    }
}
