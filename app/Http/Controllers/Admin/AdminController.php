<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAdminRequest;
use App\Models\User;
use App\Services\AdminService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
    	return view('admin.admins.index');
    }

    public function getSuperAdmins()
    {
    	$users = User::role('super_admin')->where('id', '!=', Auth::user()->id);
    	return datatables($users)->tojson();
    }

    public function create()
    {
    	return view('admin.admins.create');
    }

    public function store(CreateAdminRequest $request)
    {
    	$request->merge(['password' => bcrypt('superadmin')]);
    	User::create($request->all())->assignRole('super_admin');

    	AdminService::storeSuperAdmin($request);

    	return redirect()->route('admin.index', request()->school)->withSuccess('Administrador cadastrado com sucesso');
    }
}
