<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function firstAccess()
    {
        return view('auth.passwords.change');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = Auth::user()->update([
            'password'      => bcrypt($request->password),
            'first_access'  => 0
        ]);

        return redirect()->route('home');
    }
}
