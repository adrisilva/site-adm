<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Models\Conectados\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AlunoController extends Controller
{
	public static $courses =  [
		'Enfermagem'        => 'Enfermagem', 
		'Finanças'          => 'Finanças',
		'Informática'       => 'Informática',
		'Produção de Moda'  => 'Produção de Moda'
	];

	public static $series  = [
		'7' => '7º Ano',
		'8' => '8º Ano',
		'9' => '9º Ano',
		'1' => '1º Ano do Ensino Médio',
		'2' => '2º Ano do Ensino Médio',
		'3' => '3º Ano do Ensino Médio',
	];

  	public function index()
  	{
  		return view('admin.alunos.index'); 
  	}

	public function create()
	{
		$courses = self::$courses;
		$series = self::$series;
		
		return view('admin.alunos.create', compact('courses', 'series'));
	}

	public function store(Request $request, $school)
	{
		$user = UserService::store($request, 'Aluno');

        if (!$user['success']) {
            return redirect()
                ->back()
                ->withErrors($user['data'])
                ->withInput();
        }

		return redirect()->back()->withSuccess('Aluno cadastrado com sucesso');
	}

  	public function getAlunos()
  	{
		$user = new User;
		$users = $user->where('type', 'Aluno')->get();
		return datatables($users)->toJson();
  	}

  	public function delete(Request $request)
    {
        try{
            UserService::delete($request->email_aluno);
            return redirect()->back()->withSuccess('Dados removidos com sucesso!');
        }catch (\Exception $ex){
            return redirect()->back()->withError('Erro ao remover os dados do aluno');
        }
    }

    public function edit($school, $aluno_id)
    {
        $user = User::find($aluno_id);
        $courses = self::$courses;
        $series = self::$series;
        return view('admin.alunos.edit', compact('user', 'courses', 'series'));
    }

    public function update(Request $request, $school, $id)
    {
        $user = User::find($id);
        $user = UserService::update($request, $user->email, 'Aluno');

        if (!$user['success']) {
            return redirect()
                ->back()
                ->withErrors($user['data'])
                ->withInput();
        }

        return redirect()->back()->withSuccess('Dados atualizados com sucesso');
    }
}
