<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditContentRequest;
use App\Models\School;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeCmsController extends Controller
{
    public function edit()
    {
		$school = School::where('slug', request()->school)->first();
    	return view('admin.cms.home.edit', compact('school'));
    }

    public function update(EditContentRequest $request)
    {
        DB::beginTransaction();

        $school = School::where('slug', request()->school)->first();
    	$school->content->update($request->all());
        DB::commit();
    	
    	return redirect()->back()->withSuccess('Alterações salvas com sucesso');
    }
}
