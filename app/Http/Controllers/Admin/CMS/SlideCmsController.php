<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditSlideRequest;
use App\Models\School;
use App\Models\Slide;
use App\Services\MediaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SlideCmsController extends Controller
{
    public function index()
    {
        $school = School::where('slug', request()->school)->first();
    	$slides = $school->slides;
    	return view('admin.cms.slides.index', compact('slides'));
    }

    public function changeStatus(Request $request, $school, $slide_id)
    {
	 	DB::beginTransaction();
    	$slide = Slide::find($slide_id);
    	$slide->active = !$slide->active;
    	$slide->save();
    	DB::commit();

    	return response(200);
    }

    public function edit($school, $slide_id)
    {
        $slide = Slide::find($slide_id);
    	return view('admin.cms.slides.edit', compact('slide'));
    }

    public function update(EditSlideRequest $request, $school, $id)
    {
        $slide = Slide::find($id);

        DB::beginTransaction();
        if($request->has('image')){
            $media = MediaService::uploadMedia($request->image, 'image');
            $request->merge(['media_id' => $media->id]);
        }


        $slide->update($request->all());
        DB::commit();

        return redirect()->back()->withSuccess('Dados atualizados com sucesso');
    }
}
