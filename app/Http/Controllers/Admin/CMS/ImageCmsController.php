<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditImageRequest;
use App\Models\Image;
use App\Models\School;
use App\Services\MediaService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ImageCmsController extends Controller
{
    public function index()
    {
        $school = School::where('slug', request()->school)->first();
        $images = $school->images;
    	return view('admin.cms.images.index', compact('images'));
    }

    public function edit($school, $image_id)
    {
        $image = Image::find($image_id);
    	return view('admin.cms.images.edit', compact('image'));
    }

    public function changeStatus(Request $request, $school, $id)
    {
        DB::beginTransaction();
        $image  = Image::find($id);
        $image->active = !$image->active;
        $image->save();
        DB::commit();

        return response(200); 
    }

    public function update(EditImageRequest $request, $school, $id)
    {
        $image = Image::find($id);

        DB::beginTransaction();
        if($request->has('image')){
            $media = MediaService::uploadMedia($request->image, 'image');
            $request->merge(['media_id' => $media->id]);
        }


        $image->update($request->all());
        DB::commit();

        return redirect()->back()->withSuccess('Dados atualizados com sucesso');
    }
}
