<?php

namespace App\Http\Controllers\Admin;

use App\Services\UserService;
use App\Util\Util;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Image;
use App\Models\Slide;
use App\Models\School;
use App\Models\Content;
use App\Models\AdminLeitura;
use Illuminate\Http\Request;
use App\Mail\CreateSchoolMail;
use App\Models\AdminLinguagem;
use App\Services\AdminService;
use App\Services\ImageService;
use App\Services\SlideService;
use App\Models\AdminMatematica;
use App\Services\SchoolService;
use App\Services\ContentService;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\CreateSchoolRequest;
use App\Http\Requests\UpdateSchoolRequest;

class SchoolController extends Controller
{
    public function index()
    {
    	return view('admin.escola.index');
    }

    public function getSchools()
    {
        $schools = School::all();
        return datatables($schools)->toJson();
    }

    public function create()
    {
    	return view('admin.escola.create');
    }

    public function store(CreateSchoolRequest $request)
    {
        DB::beginTransaction();

        if(!isset($request->lt_active)){
    		$request->merge(['lt_active' => 0]);
    	}

    	if(!isset($request->lc_active)){
    		$request->merge(['lc_active' => 0]);
    	}

    	if(!isset($request->mt_active)){
    		$request->merge(['mt_active' => 0]);
        }
        
        $school = School::create([
            'name'          => $request->name,
            'inep'          => $request->inep,
            'renewal_date'  => Carbon::now()->format('Y-m-d'),
            'slug'          => $request->slug,
            'lt_active'     => $request->lt_active,
            'lc_active'     => $request->lc_active,
            'mt_active'     => $request->mt_active
        ]);

        $password = Util::generatePassword();
        $diretor = User::create([
            'name'      => $request->diretor_name,
            'email'         => $request->diretor_email,
            'phone'         => $request->diretor_tel,
            'password'   => bcrypt($password),
            'school_id' => $school->id,
            'cpf'   => $request->cpf
        ])->assignRole('diretor');

        Mail::to($diretor)->send(new CreateSchoolMail($diretor->name, $diretor->email, $password));

        $address = $school->address()->create([
            'cep'        => $request->cep,
            'address'    => $request->endereco
        ]);

        $request->merge([
            'password' => bcrypt($password)
        ]);

        SlideService::createSlides($school->id);
        ContentService::createContent($school->id);
        ImageService::createImages($school->id);

        $schoolAdminCreate = SchoolService::createSchools($request, $diretor, $password);

        if (!$schoolAdminCreate['success']) {
            return redirect()
                        ->back()
                        ->withErrors($schoolAdminCreate['data'])
                        ->withInput();
        }

        DB::commit();

        return redirect()->route('school.index')->withSuccess('Escola Cadastrada com sucesso');
    }

    public function edit($slug)
    {
        $school = School::where('slug', $slug)->first();

        $director = User::role('diretor')->where('school_id', $school->id)->first();

        $admin_lt = AdminService::getAdminLeitura($slug);
        $admin_lc = AdminService::getAdminLinguagem($slug);
        $admin_mt = AdminService::getAdminMatematica($slug);

        return view('admin.escola.edit', compact('slug', 'school', 'director', 'admin_lt', 'admin_lc', 'admin_mt'));
    }

    public function update($slug, UpdateSchoolRequest $request)
    {
        DB::beginTransaction();

        if(!isset($request->lt_active)){
    		$request->merge(['lt_active' => 0]);
    	}

    	if(!isset($request->lc_active)){
    		$request->merge(['lc_active' => 0]);
    	}

    	if(!isset($request->mt_active)){
    		$request->merge(['mt_active' => 0]);
        }

        $school = School::where('slug', $slug)->first();

        $school->update([
            'name'          => $request->name,
            'inep'          => $request->inep,
            'slug'          => $request->slug,
            'lt_active'     => $request->lt_active,
            'lc_active'     => $request->lc_active,
            'mt_active'     => $request->mt_active
        ]);

        $address = $school->address()->update([
            'cep'        => $request->cep,
            'address'    => $request->endereco
        ]);

        $diretor = User::where('email', $request->email)->first();
        if(!empty($diretor)){
           $data = [
                'name'          => $request->diretor_name,
                'phone'         => $request->diretor_tel,
                'cpf'           =>  $request->cpf,
            ];
        }else{
            $password = Util::generatePassword();

            Mail::to($request->email)->send(new CreateSchoolMail($request->diretor_name,  $request->email, $password));

            $data = [
                'name'          => $request->diretor_name,
                'email'         => $request->email,
                'phone'         => $request->diretor_tel,
                'cpf'           =>  $request->cpf,
                'password'      => bcrypt($password)
            ];

            $request->merge([
                'password'  => bcrypt($password)
            ]);
        }

        $director = User::role('diretor')->where('school_id', $school->id)->update($data);



        SchoolService::updateSchools($slug, $request);
        AdminService::updateAdmins($slug, $request);
        AdminService::updateDiretor($slug, $request);

        DB::commit();

        return redirect()->route('school.index')->withSuccess('Dados Alterados com sucesso');
    }

    public function delete($slug)
    {
        DB::beginTransaction();
        $school = School::where('slug', $slug)->delete();
        SchoolService::deleteSchools($slug);
        DB::commit();

        return redirect()->route('school.index')->withSuccess('Dados da escola apagados com sucesso');
    }

    public function firstAccessCount()
    {
        return view('admin.escola.first_access');
    }

    public function countFirstAccessUsers()
    {
        $schoolUsers = UserService::countFirstAccess();
        return datatables($schoolUsers)->toJson();
    }
}
