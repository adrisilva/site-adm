<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class DirectorController extends Controller
{
    public function index()
    {
    	return view('admin.diretores.index');
    }

    public function getDirectors()
    {
		$users = User::role('diretor')->with('school')->get();
        
    	return datatables($users)->tojson();
    }
}
